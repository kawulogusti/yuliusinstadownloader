package com.video.photo.downloader.instagram.bulk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UsersItem extends RealmObject implements Parcelable {

    public static final Parcelable.Creator<UsersItem> CREATOR = new Parcelable.Creator<UsersItem>() {
        @Override
        public UsersItem createFromParcel(Parcel source) {
            return new UsersItem(source);
        }

        @Override
        public UsersItem[] newArray(int size) {
            return new UsersItem[size];
        }
    };
    @SerializedName("profile_image_url_https")
    private String profileImageUrlHttps;
    @SerializedName("id_str")
    private String idStr;
    @SerializedName("name")
    private String name;
    @PrimaryKey
    @SerializedName("id")
    private long id;
    @SerializedName("username")
    private String username;

    public UsersItem(String profileImageUrlHttps, String idStr, String name, long id, String username) {
        this.profileImageUrlHttps = profileImageUrlHttps;
        this.idStr = idStr;
        this.name = name;
        this.id = id;
        this.username = username;
    }

    public UsersItem() {
    }

    protected UsersItem(Parcel in) {
        this.profileImageUrlHttps = in.readString();
        this.idStr = in.readString();
        this.name = in.readString();
        this.id = in.readLong();
        this.username = in.readString();
    }

    public String getProfileImageUrlHttps() {
        return profileImageUrlHttps;
    }

    public void setProfileImageUrlHttps(String profileImageUrlHttps) {
        this.profileImageUrlHttps = profileImageUrlHttps;
    }

    public String getIdStr() {
        return idStr;
    }

    public void setIdStr(String idStr) {
        this.idStr = idStr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return
                "UsersItem{" +
                        "profile_image_url_https = '" + profileImageUrlHttps + '\'' +
                        ",id_str = '" + idStr + '\'' +
                        ",name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        ",username = '" + username + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.profileImageUrlHttps);
        dest.writeString(this.idStr);
        dest.writeString(this.name);
        dest.writeLong(this.id);
        dest.writeString(this.username);
    }
}