package com.video.photo.downloader.instagram.bulk.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.model.Download;
import com.video.photo.downloader.instagram.bulk.model.MediasItem;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;

public class HelperUtils {

    private Context mContext;
    private String mType;
    private int current = 0;
    private int total = 0;
    private onDeleteListener listener;
    private ArrayList<MediasItem> mediasItems = new ArrayList<>();

    public HelperUtils(Context context, String type) {
        this.mContext = context;
        this.listener = null;
        this.mType = type;
    }

    public HelperUtils(Context context, String type, ArrayList<MediasItem> mediasItems) {
        this.mContext = context;
        this.listener = null;
        this.mType = type;
        this.mediasItems = mediasItems;
    }

    public static boolean isValidUrl(String url) {
        try {
            new URL(url).toURI();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isFileExist(Context context, String file_url) {
        String mBaseFolderPath = android.os.Environment
                .getExternalStorageDirectory()
                + File.separator
                + context.getResources().getString(R.string.app_name) + File.separator;
        if (!new File(mBaseFolderPath).exists()) {
            new File(mBaseFolderPath).mkdir();
        }
        assert file_url != null;
        String filename = new File(file_url).getName().split("\\?")[0];
        File file = new File(mBaseFolderPath + "/" + filename);
        return file.exists();
    }

    public static String getFilePath(Context context, String file_url) {
        String mBaseFolderPath = android.os.Environment
                .getExternalStorageDirectory()
                + File.separator
                + context.getResources().getString(R.string.app_name) + File.separator;
        if (!new File(mBaseFolderPath).exists()) {
            new File(mBaseFolderPath).mkdir();
        }
        assert file_url != null;
        String filename = new File(file_url).getName().split("\\?")[0];
        File file = new File(mBaseFolderPath + "/" + filename);
        return file.getAbsolutePath();
    }

    public static String getFileNameFromPath(Context context, String file_url) {
        String mBaseFolderPath = android.os.Environment
                .getExternalStorageDirectory()
                + File.separator
                + context.getResources().getString(R.string.app_name) + File.separator;
        if (!new File(mBaseFolderPath).exists()) {
            new File(mBaseFolderPath).mkdir();
        }
        assert file_url != null;
        return new File(file_url).getName().split("\\?")[0];
    }

    public static boolean getRandomBoolean() {
        return Math.random() < 0.5;
    }

    public void setOnDeleteListener(onDeleteListener listener) {
        this.listener = listener;
        showDeleteDialog(mContext, mType);
    }

    public void showDeleteDialog(Context context, String type) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (mediasItems != null && mediasItems.size() > 0) {
            if (type.equalsIgnoreCase(Constants.DOWNLOADS)) {
                builder.setMessage(R.string.text_delete_confirmation_downloads_items);
            } else if (type.equalsIgnoreCase(Constants.FAVORITES)) {
                builder.setMessage(R.string.text_delete_confirmation_favorites_items);
            } else if (type.equalsIgnoreCase(Constants.HISTORY)) {
                builder.setMessage(R.string.text_delete_confirmation_history_items);
            }
        } else {
            if (type.equalsIgnoreCase(Constants.DOWNLOADS)) {
                builder.setMessage(R.string.text_delete_confirmation_downloads);
            } else if (type.equalsIgnoreCase(Constants.FAVORITES)) {
                builder.setMessage(R.string.text_delete_confirmation_favorites);
            } else if (type.equalsIgnoreCase(Constants.HISTORY)) {
                builder.setMessage(R.string.text_delete_confirmation_history);
            }
        }

        builder.setPositiveButton("Yes, delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (mediasItems != null && mediasItems.size() > 0) {
                    if (type.equalsIgnoreCase(Constants.DOWNLOADS)) {
//                        Toast.makeText(context, "Please wait. Deleting files..", Toast.LENGTH_SHORT).show();
                        total = mediasItems.size();
                        int currentProgress = 0;
                        for (MediasItem mediasItem : mediasItems) {
                            currentProgress = currentProgress + 1;
                            current = currentProgress;
                            File file = new File(mediasItem.getDownloadItem(context).getFilePath());
                            String filename = HelperUtils.getFileNameFromPath(mContext, mediasItem.getDownloadItem(context).getFilePath());
                            if (file.isFile()) {
                                if (filename.contains(".mp4")) {
                                    mContext.getContentResolver().delete(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                                            MediaStore.Video.Media.DATA + "='"
                                                    + file.getAbsolutePath() + "'", null);
                                } else {
                                    mContext.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                            MediaStore.Video.Media.DATA + "='"
                                                    + file.getAbsolutePath() + "'", null);
                                }
                                file.delete();
                                if (listener != null)
                                    listener.onProgressTask(current, total);
                            }
                            DBHelper.deleteDownload(mediasItem);
                        }

                        if (listener != null)
                            listener.onCompleteTask(true);
                    } else if (type.equalsIgnoreCase(Constants.FAVORITES)) {
                        total = mediasItems.size();
                        int currentProgress = 0;
                        for (MediasItem mediasItem : mediasItems) {
                            currentProgress = currentProgress + 1;
                            current = currentProgress;
                            if (listener != null)
                                listener.onProgressTask(current, total);
                            DBHelper.deleteFavorite(mediasItem);
                        }
                        if (listener != null)
                            listener.onCompleteTask(true);
                    } else if (type.equalsIgnoreCase(Constants.HISTORY)) {
                        total = mediasItems.size();
                        int currentProgress = 0;
                        for (MediasItem mediasItem : mediasItems) {
                            currentProgress = currentProgress + 1;
                            current = currentProgress;
                            if (listener != null)
                                listener.onProgressTask(current, total);
                            DBHelper.deleteHistory(mediasItem);
                        }
                        if (listener != null)
                            listener.onCompleteTask(true);
                    }
                } else {
                    if (type.equalsIgnoreCase(Constants.DOWNLOADS)) {
                        Toast.makeText(context, "Please wait. Deleting files..", Toast.LENGTH_SHORT).show();
                        total = DBHelper.getDownloadList().size();
                        int currentProgress = 0;
                        for (Download download : DBHelper.getDownloadList()) {
                            currentProgress = currentProgress + 1;
                            current = currentProgress;
                            File file = new File(download.getFilePath());
                            String filename = HelperUtils.getFileNameFromPath(mContext, download.getFilePath());
                            if (file.isFile()) {
                                if (filename.contains(".mp4")) {
                                    mContext.getContentResolver().delete(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                                            MediaStore.Video.Media.DATA + "='"
                                                    + file.getAbsolutePath() + "'", null);
                                } else {
                                    mContext.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                            MediaStore.Video.Media.DATA + "='"
                                                    + file.getAbsolutePath() + "'", null);
                                }
//                                    file.delete();
                                if (listener != null)
                                    listener.onProgressTask(current, total);
                            }
                        }
                        DBHelper.deleteAllDownloads();
                        String mBaseFolderPath = Environment
                                .getExternalStorageDirectory()
                                + File.separator
                                + mContext.getResources().getString(R.string.app_name) + File.separator;
                        if (!new File(mBaseFolderPath).exists()) {
                            new File(mBaseFolderPath).mkdir();
                        }
                        File directory = new File(mBaseFolderPath);
                        for (File tempFile : directory.listFiles()) {
                            tempFile.delete();
                        }
                        if (listener != null)
                            listener.onCompleteTask(true);
                    } else if (type.equalsIgnoreCase(Constants.FAVORITES)) {
                        DBHelper.deleteAllFavorites();
                        if (listener != null)
                            listener.onCompleteTask(true);
                    } else if (type.equalsIgnoreCase(Constants.HISTORY)) {
                        DBHelper.deleteAllHistory();
                        if (listener != null)
                            listener.onCompleteTask(true);
                    }
                }


            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    public interface onDeleteListener {
        public void onCompleteTask(boolean status);

        public void onProgressTask(int current, int total);
    }
}
