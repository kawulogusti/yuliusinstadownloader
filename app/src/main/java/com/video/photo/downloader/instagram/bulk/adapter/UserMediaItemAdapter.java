package com.video.photo.downloader.instagram.bulk.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.activity.PreviewActivity;
import com.video.photo.downloader.instagram.bulk.activity.UsermediaActivity;
import com.video.photo.downloader.instagram.bulk.model.MediasItem;
import com.video.photo.downloader.instagram.bulk.utils.GlideApp;
import com.video.photo.downloader.instagram.bulk.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ahm on 11/05/17.
 */

public class UserMediaItemAdapter extends RecyclerView.Adapter<UserMediaItemAdapter.MyViewHolder> {

    private Context mContext;
    private List<MediasItem> listKonten;
    private UsermediaActivity usermediaActivity;
    private boolean isSelectedAllEnabled = false;

    public UserMediaItemAdapter(Context mContext, List<MediasItem> listKontenP) {
        this.mContext = mContext;
        this.listKonten = listKontenP;
        this.usermediaActivity = (UsermediaActivity) mContext;
    }

    public boolean isSelectedAllEnabled() {
        return isSelectedAllEnabled;
    }

    public void setSelectedAllEnabled(boolean selectedAllEnabled) {
        if (selectedAllEnabled) {
            isSelectedAllEnabled = selectedAllEnabled;
            for (int i = 0; i < getItemCount(); i++) {
                listKonten.get(i).setSelected(true);
            }
        } else {
            isSelectedAllEnabled = selectedAllEnabled;
            for (int i = 0; i < getItemCount(); i++) {
                listKonten.get(i).setSelected(false);
            }
        }

    }

    public ArrayList<MediasItem> getSelectedMediaItems() {
        ArrayList<MediasItem> mediasItemsLocal = new ArrayList<>();
        for (MediasItem mediasItem : listKonten) {
            if (mediasItem.isSelected()) {
                LogUtils.log("Found selected: " + mediasItem.getId());
                mediasItemsLocal.add(mediasItem);
            }
        }
        return mediasItemsLocal;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_usermediaitem, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final MediasItem mediasItem = listKonten.get(position);

        GlideApp
                .with(mContext)
                .load(mediasItem.getDisplay_url())
                .error(R.drawable.ic_broken_image_black_24dp)
                .into(holder.iv_image);

        if (mediasItem.getUrl().contains(".mp4")) {
            holder.iv_type.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_videocam_black_24dp));
        } else {
            holder.iv_type.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_image_black_24dp));
        }

        if (isSelectedAllEnabled) {
            holder.cb_selected.setChecked(true);
        } else {
            holder.cb_selected.setChecked(false);
        }


        holder.cb_selected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LogUtils.log("Selected: " + isChecked);
                if (isChecked) {
                    listKonten.get(position).setSelected(true);
                } else {
                    listKonten.get(position).setSelected(false);
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usermediaActivity.showIntersialAds();
                Intent intent = new Intent(mContext, PreviewActivity.class);
                intent.putExtra(PreviewActivity.CURRENT_MEDIA_ITEM, mediasItem);
                intent.putParcelableArrayListExtra(PreviewActivity.MEDIA_LIST, new ArrayList<MediasItem>(listKonten));
                intent.putExtra(PreviewActivity.CURRENT_POSITION, position);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listKonten.size();
    }

    /*private void showPopupMenu(UsersItem konten, View view) {
        PopupMenu popup = new PopupMenu(mContext, view);
        //inflating menu from xml resource
        popup.inflate(R.menu.song_options_menu);
        Menu menuOpts = popup.getMenu();

        Favorite favorite = DBHelper.getFavorite(konten.getMediaId());
        if (favorite != null) {
            menuOpts.getItem(0).setTitle(R.string.remove_favorite);
        } else {
            menuOpts.getItem(0).setTitle(R.string.action_favorite);
        }
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_playlist:
                        //handle menu2 click
                        usermediaActivity.showAddToPlaylistDialog(konten);
//                        Toast.makeText(mContext, "Coming soon..", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.action_favorite:
                        //handle menu2 click
                        DBHelper.setOrDeleteFavorite(konten);
                        if (favorite != null) {
                            Toast.makeText(mContext, "Removed from favorite", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, "Added to favorite", Toast.LENGTH_SHORT).show();
                        }
                        notifyDataSetChanged();
                        return true;

                    default:
                        return false;
                }
            }
        });
        //displaying the popup
        popup.show();
    }
*/

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_image, iv_type; //, iv_download, iv_more;
        private TextView tv_name, tv_username;
        private CheckBox cb_selected;

        public MyViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.iv_image);
            iv_type = view.findViewById(R.id.iv_type);
            cb_selected = view.findViewById(R.id.cb_selected);
        }
    }


}