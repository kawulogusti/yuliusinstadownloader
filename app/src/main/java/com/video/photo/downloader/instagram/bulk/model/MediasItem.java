package com.video.photo.downloader.instagram.bulk.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.video.photo.downloader.instagram.bulk.utils.HelperUtils;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class MediasItem extends RealmObject implements Parcelable {

    public static final Creator<MediasItem> CREATOR = new Creator<MediasItem>() {
        @Override
        public MediasItem createFromParcel(Parcel source) {
            return new MediasItem(source);
        }

        @Override
        public MediasItem[] newArray(int size) {
            return new MediasItem[size];
        }
    };
    @SerializedName("profile_image_url_https")
    private String profileImageUrlHttps;
    @SerializedName("name")
    private String name;
    @SerializedName("idStr")
    private String idStr;
    @SerializedName("expanded_url")
    private String expandedUrl;
    @SerializedName("display_url")
    private String display_url;
    @PrimaryKey
    @SerializedName("id")
    private String id;
    @SerializedName("text")
    private String text;
    @SerializedName("url")
    private String url;
    @SerializedName("username")
    private String username;
    @SerializedName("media_url_https")
    private String mediaUrlHttps;
    private boolean isSelected;
    private int downloadId;

    public MediasItem(String profileImageUrlHttps, String name, String idStr, String expandedUrl, String display_url, String id, String text, String url, String username, String mediaUrlHttps) {
        this.profileImageUrlHttps = profileImageUrlHttps;
        this.name = name;
        this.idStr = idStr;
        this.expandedUrl = expandedUrl;
        this.display_url = display_url;
        this.id = id;
        this.text = text;
        this.url = url;
        this.username = username;
        this.mediaUrlHttps = mediaUrlHttps;
    }

    public MediasItem() {
    }

    protected MediasItem(Parcel in) {
        this.profileImageUrlHttps = in.readString();
        this.name = in.readString();
        this.expandedUrl = in.readString();
        this.display_url = in.readString();
        this.id = in.readString();
        this.idStr = in.readString();
        this.text = in.readString();
        this.url = in.readString();
        this.username = in.readString();
        this.mediaUrlHttps = in.readString();
    }

    public String getIdStr() {
        return idStr;
    }

    public void setIdStr(String idStr) {
        this.idStr = idStr;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getDownloadId() {
        return downloadId;
    }

    public void setDownloadId(int downloadId) {
        this.downloadId = downloadId;
    }

    public int getNotificationId() {
        return 98345;
    }

    public String getDisplay_url() {
        return display_url;
    }

    public void setDisplay_url(String display_url) {
        this.display_url = display_url;
    }

    public Favorite getFavoriteItem() {
        return new Favorite(this.profileImageUrlHttps, this.name, this.expandedUrl, this.display_url, this.id, this.idStr, this.text, this.url, this.username, this.mediaUrlHttps, new Date());
    }

    public History getHistoryItem() {
        return new History(this.profileImageUrlHttps, this.name, this.expandedUrl, this.display_url, this.id, this.idStr, this.text, this.url, this.username, this.mediaUrlHttps, new Date());
    }

    public Download getDownloadItem(Context context) {
        return new Download(this.profileImageUrlHttps, this.name, this.expandedUrl, this.display_url, this.id, this.idStr, this.text, this.url, HelperUtils.getFilePath(context, this.getUrl()), this.username, this.mediaUrlHttps, new Date());
    }

    public UsersItem getUserItem() {
        return new UsersItem(this.profileImageUrlHttps, this.idStr, this.name, 0, this.username);
    }

    public String getMediaUrlHttps() {
        return mediaUrlHttps;
    }

    public void setMediaUrlHttps(String mediaUrlHttps) {
        this.mediaUrlHttps = mediaUrlHttps;
    }

    public String getProfileImageUrlHttps() {
        return profileImageUrlHttps;
    }

    public void setProfileImageUrlHttps(String profileImageUrlHttps) {
        this.profileImageUrlHttps = profileImageUrlHttps;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpandedUrl() {
        return expandedUrl;
    }

    public void setExpandedUrl(String expandedUrl) {
        this.expandedUrl = expandedUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return
                "MediasItem{" +
                        "profile_image_url_https = '" + profileImageUrlHttps + '\'' +
                        ",name = '" + name + '\'' +
                        ",expanded_url = '" + expandedUrl + '\'' +
                        ",id = '" + id + '\'' +
                        ",idStr = '" + idStr + '\'' +
                        ",text = '" + text + '\'' +
                        ",url = '" + url + '\'' +
                        ",media_url_https = '" + mediaUrlHttps + '\'' +
                        ",username = '" + username + '\'' +
                        ",display_url = '" + display_url + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.profileImageUrlHttps);
        dest.writeString(this.name);
        dest.writeString(this.expandedUrl);
        dest.writeString(this.display_url);
        dest.writeString(this.id);
        dest.writeString(this.idStr);
        dest.writeString(this.text);
        dest.writeString(this.url);
        dest.writeString(this.username);
        dest.writeString(this.mediaUrlHttps);
    }
}