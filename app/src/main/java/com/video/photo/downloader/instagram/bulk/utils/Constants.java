package com.video.photo.downloader.instagram.bulk.utils;

public class Constants {

    public static final String RATING_OPTIONS = "rating_options";
    public static final String DOWNLOADS = "downlaods";
    public static final String FAVORITES = "favorites";
    public static final String HISTORY = "history";
    public static final String BASE_URL_DB = "https://www.dropbox.com/";
    public static final String CHANNEL_ID = "instagram_downloader_bulk";

    public static final String PREMIUM_USER_REMOVE_ADS = "premium_user_remove_ads";
//        public static final String REMOVE_ADS_TEXT_ID = "android.test.purchased";
    public static final String REMOVE_ADS_TEXT_ID = "remove_ads";
    public static final String IS_PREMIUM_USER_REMOVE_ADS = "is_premium_user_remove_ads";
    public static final String UPGRADE_REMOVE_ADS_OPTIONS = "upgrade_remove_ads";
    public static final String WEBVIEW_USER_AGENT = "Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Mobile Safari/537.36";
    public static final String LAST_URL = "last_url";
}
