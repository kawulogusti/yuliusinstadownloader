package com.video.photo.downloader.instagram.bulk.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.api.ConfApp;
import com.video.photo.downloader.instagram.bulk.model.Download;
import com.video.photo.downloader.instagram.bulk.model.MediasItem;
import com.video.photo.downloader.instagram.bulk.service.DownloadService;
import com.video.photo.downloader.instagram.bulk.utils.AppRaterHelper;
import com.video.photo.downloader.instagram.bulk.utils.Constants;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.HelperUtils;
import com.video.photo.downloader.instagram.bulk.utils.LogUtils;
import com.video.photo.downloader.instagram.bulk.utils.SharedPref;

import java.util.ArrayList;
import java.util.List;

public class StorydownloaderActivity extends AppCompatActivity {

    public static final String POST_URL = "post_url";
    private static final String IS_DIALOG_INFO_LOADED = "is_first_dialog_loaded";

    private Button btn_go;
    private ImageButton btn_download, btn_back, btn_forward;
    private EditText editTextUrl;
    private ProgressBar progressBar;
    private WebView webView;

    //ads
    private AdView mAdView;
    private AdRequest adRequest;
    private InterstitialAd mInterstitialAd;

    private ConfApp confApp;

    //
    String urlBrowser = "";
    String urlFinished = "";
    List<MediasItem> mediasItemList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storydownloader);
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Story Downloader");

        confApp = DBHelper.getConfApp();

        btn_back = findViewById(R.id.btn_back);
        btn_forward = findViewById(R.id.btn_forward);
        btn_go = findViewById(R.id.btn_go);
        btn_download = findViewById(R.id.btn_download);
        editTextUrl = findViewById(R.id.editTextUrl);
        progressBar = findViewById(R.id.progressBar);
        webView = findViewById(R.id.webView);

        //ads
        mAdView = findViewById(R.id.adView);

        confApp = DBHelper.getConfApp();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));

            mAdView.setVisibility(View.VISIBLE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);

            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.google_admob_intersial_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    requestIntersialAds();
                    if (HelperUtils.getRandomBoolean()) {
                        AppRaterHelper.showReviewDialog(StorydownloaderActivity.this);
                    }
                }
            });


        } else {
            mAdView.setVisibility(View.GONE);
        }

        if (!SharedPref.getBol(IS_DIALOG_INFO_LOADED)) {
            Toast.makeText(this, "Please login first to use Story Downloader feature. Then, go to a story and tap on the download button.", Toast.LENGTH_LONG).show();
            SharedPref.saveBol(IS_DIALOG_INFO_LOADED, true);
        }


        webView.clearCache(true);
        Intent intent = getIntent();
        String urlFromSearch = intent.getStringExtra(POST_URL);
        if (urlFromSearch != null) {
            webView.loadUrl(urlFromSearch);
        } else {
            String lastUrl = SharedPref.getString(Constants.LAST_URL);
            if (lastUrl != null) {
                webView.loadUrl(lastUrl);
            } else {

                webView.loadUrl("https://instagram.com");
            }

        }


        editTextUrl.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) &&
                        (i == KeyEvent.KEYCODE_ENTER)) {
                    goToSearch();
                    return true;
                }
                return false;
            }
        });

        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToSearch();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webView.canGoBack()) {
                    webView.goBack();
                }
            }
        });

        btn_forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webView.canGoForward()) {
                    webView.goForward();
                }
            }
        });


        btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                downloadAction();
            }
        });



        webView.getSettings().setUserAgentString(Constants.WEBVIEW_USER_AGENT);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.addJavascriptInterface(this, "Downloader");

        webView.setWebViewClient(new WebViewClient(){


            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                Log.d("VideoDownload", "Current url = "+url);
                editTextUrl.setText(url);
                urlBrowser = url;
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                /*if (url.contains(".jpg?") || url.contains(".mp4?")) {
                    view.loadUrl("javascript:( window.onload=function() {"
//                            +" var button = document.querySelector('button[class*=\"videoSpritePlayButton\"]'); button.click();"
                            + "var el = document.querySelector('source[src^=\"http\"]');"
                            + "var elImg = document.querySelector('img[srcset^=\"http\"]');"
                            + "var user = document.querySelector('a[class=\"FPmhX notranslate  R4sSg\"]');"
                            + "var username = user.title;"
//                            + "var url = el.src;"
//                            + "var urlImg = elImg.srcset.split(','); urlImg = urlImg[2].replace(' 1080w','');"
                            + "if (el != null && elImg != null ) { var url = el.src; var urlImg = 'http'; var filename = url.split('/').pop().split('#')[0].split('?')[0]; } else if (el == null && elImg != null) {  var urlImg = elImg.srcset.split(','); urlImg = urlImg[2].replace(' 1080w',''); var filename = urlImg.split('/').pop().split('#')[0].split('?')[0]; var url = 'http'; }  "
//                            + "if (el != null && elImg != null ) { var url = el.src; var urlImg = elImg.srcset.split(','); urlImg = urlImg[2].replace(' 1080w','');  var filename = url.split('/').pop().split('#')[0].split('?')[0]; } else if (el == null && elImg != null) {  var urlImg = elImg.srcset.split(','); urlImg = urlImg[2].replace(' 1080w',''); var filename = urlImg.split('/').pop().split('#')[0].split('?')[0]; var url = 'http'; }  "
//                            + "if (elImg != null ) { var urlImg = elImg.content; var filename = urlImg.split('/').pop().split('#')[0].split('?')[0];} else { var urlImg = 'http'; var filename = urlImg.split('/').pop().split('#')[0].split('?')[0];} "
                            +" console.log(url is: "+url+");"
                            +" console.log(urlImg);"
                            +" console.log(filename);"
                            +" Downloader.processStory(username, url);"
                            +" Downloader.processStory(username, urlImg);"
                            + "}"
                            + ")()");
                }*/
                super.onLoadResource(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                LogUtils.log("Finished url = "+url);
                progressBar.setVisibility(View.GONE);
                SharedPref.saveString(Constants.LAST_URL, url);
                editTextUrl.setText(url);

                /*if ((url.contains("instagram.com/p/") || url.contains("instagram.com/tv/")) && !url.contains("igshid=") && !url.contains("utm_source=ig_web_copy_link")) {
//                    url = mainActivity.rewriteUrl(url);
                    Log.d("VideoDownload", "Redirecting url = "+url);
                    view.clearCache(true);
                    view.loadUrl(url);
//                    isPostImagesLoaded = url.contains("instagram.com/p/");
                } else {
                    urlBrowser = url;
                    urlFinished = url;
                }*/

                if (url.contains("instagram.com/stories/")) {
                    view.loadUrl("javascript:( window.onload=function() {"
//                            +" var button = document.querySelector('button[class*=\"videoSpritePlayButton\"]'); button.click();"
                            + "var el = document.querySelector('source[src^=\"http\"]');"
                            + "var elImg = document.querySelector('img[srcset^=\"http\"]');"
                            + "var user = document.querySelector('a[class=\"FPmhX notranslate  R4sSg\"]');"
                            + "var username = user.title;"
//                            + "var urlSource = window.location.href;"
                            + "var urlSource = document.URL;"
//                            + "var url = el.src;"
//                            + "var urlImg = elImg.srcset.split(','); urlImg = urlImg[2].replace(' 1080w','');"
                            + "if (el != null && elImg != null ) { var url = el.src; var urlImg = 'http'; var filename = url.split('/').pop().split('#')[0].split('?')[0]; } else if (el == null && elImg != null) {  var urlImg = elImg.srcset.split(','); urlImg = urlImg[2].replace(' 1080w',''); var filename = urlImg.split('/').pop().split('#')[0].split('?')[0]; var url = 'http'; }  "
//                            + "if (el != null && elImg != null ) { var url = el.src; var urlImg = elImg.srcset.split(','); urlImg = urlImg[2].replace(' 1080w','');  var filename = url.split('/').pop().split('#')[0].split('?')[0]; } else if (el == null && elImg != null) {  var urlImg = elImg.srcset.split(','); urlImg = urlImg[2].replace(' 1080w',''); var filename = urlImg.split('/').pop().split('#')[0].split('?')[0]; var url = 'http'; }  "
//                            + "if (elImg != null ) { var urlImg = elImg.content; var filename = urlImg.split('/').pop().split('#')[0].split('?')[0];} else { var urlImg = 'http'; var filename = urlImg.split('/').pop().split('#')[0].split('?')[0];} "
                            +" console.log(url);"
                            +" console.log(urlImg);"
                            +" console.log(filename);"
                            +" Downloader.processStory(username, url, urlSource);"
                            +" Downloader.processStory(username, urlImg, urlSource);"
                            + "}"
                            + ")()");
                }
                super.onPageFinished(view, url);

            }
        });

        webView.setWebChromeClient(new WebChromeClient(){

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                progressBar.setMax(100);
                if (newProgress < 100) {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setProgress(newProgress);
                } else {
                    progressBar.setProgress(newProgress);
                    progressBar.setVisibility(View.GONE);
                }
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d("VideoDownload", "Console browser = "+consoleMessage.message());
                return true;
            }
        });
    }

    public void showIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            if (mInterstitialAd.isLoaded() && HelperUtils.getRandomBoolean()) {
                mInterstitialAd.show();
            } else {
                requestIntersialAds();
                if (HelperUtils.getRandomBoolean()) {
                    AppRaterHelper.showReviewDialog(StorydownloaderActivity.this);
                }
                //Log.d("TAG", "The interstitial wasn't loaded yet.");
            }
        }
    }

    private void goToSearch() {
        String url = editTextUrl.getText().toString();
        if (!url.isEmpty()) {
            if (HelperUtils.isValidUrl(url)) {
                webView.loadUrl(url);
            } else {
                String query = url.replace(" ","+");
                url = "https://google.com/search?q=site%3Ainstagram.com+"+query;
                webView.loadUrl(url);
            }

        } else {
            webView.loadUrl("https://instagram.com");
        }
    }

    @JavascriptInterface
    public void processStory(String username, String url, String urlSource)
    {
//        LogUtils.log("Grabbed data is: username: "+username+" - url: "+url+" - urlSource: "+urlSource);
        if (HelperUtils.isValidUrl(url)) {
            if (!url.equalsIgnoreCase("http") && !url.equalsIgnoreCase("undefined")) {
                String filename = HelperUtils.getFileNameFromPath(StorydownloaderActivity.this, url);
                MediasItem mediasItem = new MediasItem(url, username, filename, urlSource, url, filename, "Story text", url, username, url);
                if (!mediasItemList.contains(mediasItem)) {
                    LogUtils.log("Adding to list: "+mediasItem.getId());
                    mediasItemList.add(mediasItem);
                    final Animation animShake = AnimationUtils.loadAnimation(StorydownloaderActivity.this, R.anim.shake);
                    btn_download.startAnimation(animShake);
                    btn_download.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Download download = DBHelper.getDownload(mediasItem.getId());
                            if (download == null) {
                                Intent intent = new Intent(DownloadService.BROADCAST_START_DOWNLOAD_FILE);
                                intent.putExtra(DownloadService.FILE_URL, mediasItem);
                                sendBroadcast(intent);
                            } else {
                                Toast.makeText(StorydownloaderActivity.this, filename+ " - File already downloaded.", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
                }
            }
        }
    }

    public void requestIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

}
