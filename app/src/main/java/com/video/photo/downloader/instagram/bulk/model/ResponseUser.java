package com.video.photo.downloader.instagram.bulk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ResponseUser implements Parcelable {

    public static final Parcelable.Creator<ResponseUser> CREATOR = new Parcelable.Creator<ResponseUser>() {
        @Override
        public ResponseUser createFromParcel(Parcel source) {
            return new ResponseUser(source);
        }

        @Override
        public ResponseUser[] newArray(int size) {
            return new ResponseUser[size];
        }
    };
    @SerializedName("message")
    private String message;
    @SerializedName("users")
    private List<UsersItem> users;
    @SerializedName("status")
    private int status;

    public ResponseUser() {
    }

    protected ResponseUser(Parcel in) {
        this.message = in.readString();
        this.users = new ArrayList<UsersItem>();
        in.readList(this.users, UsersItem.class.getClassLoader());
        this.status = in.readInt();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UsersItem> getUsers() {
        return users;
    }

    public void setUsers(List<UsersItem> users) {
        this.users = users;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "ResponseUser{" +
                        "message = '" + message + '\'' +
                        ",users = '" + users + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeList(this.users);
        dest.writeInt(this.status);
    }
}