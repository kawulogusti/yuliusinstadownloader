package com.video.photo.downloader.instagram.bulk.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.adapter.DownloadItemAdapter;
import com.video.photo.downloader.instagram.bulk.adapter.DownloadItemGridAdapter;
import com.video.photo.downloader.instagram.bulk.api.ConfApp;
import com.video.photo.downloader.instagram.bulk.model.Download;
import com.video.photo.downloader.instagram.bulk.model.MediasItem;
import com.video.photo.downloader.instagram.bulk.utils.AppRaterHelper;
import com.video.photo.downloader.instagram.bulk.utils.Constants;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.HelperUtils;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

public class DownloadsActivity extends AppCompatActivity {

    private TextView tv_status;
    private RecyclerView recyclerView;
    private DownloadItemAdapter adapter;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    private List<MediasItem> mediasItems = new ArrayList<>();


    private ConfApp confApp;
    //ads

    private AdView mAdView;
    private AdRequest adRequest;
    private InterstitialAd mInterstitialAd;

    private LinearLayoutManager linearLayoutManager;
    private GridLayoutManager gridLayoutManager;
    private DownloadItemGridAdapter gridAdapter;
    private boolean isGridAvailable = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloads);
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Downloads");

        progressDialog = new ProgressDialog(DownloadsActivity.this);

        tv_status = findViewById(R.id.tv_status);
        recyclerView = findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progress_circular);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);
        tv_status.setText(R.string.loading);
        fetchData();

        //ads
        mAdView = findViewById(R.id.adView);

        confApp = DBHelper.getConfApp();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));

            mAdView.setVisibility(View.VISIBLE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);

            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.google_admob_intersial_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    requestIntersialAds();
                    if (HelperUtils.getRandomBoolean()) {
                        AppRaterHelper.showReviewDialog(DownloadsActivity.this);
                    }
                }
            });


        } else {
            mAdView.setVisibility(View.GONE);
        }
    }

    public void showIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            if (mInterstitialAd.isLoaded() && HelperUtils.getRandomBoolean()) {
                mInterstitialAd.show();
            } else {
                requestIntersialAds();
                if (HelperUtils.getRandomBoolean()) {
                    AppRaterHelper.showReviewDialog(DownloadsActivity.this);
                }
                //Log.d("TAG", "The interstitial wasn't loaded yet.");
            }
        }
    }

    public void requestIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }

    public void fetchData() {
        mediasItems = getMediaItemsFromDBList(DBHelper.getDownloadList());
        gridAdapter = new DownloadItemGridAdapter(this, mediasItems);
        recyclerView.setAdapter(gridAdapter);
        if (mediasItems.size() > 0) {
            progressBar.setVisibility(View.GONE);
            tv_status.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            tv_status.setVisibility(View.VISIBLE);
            tv_status.setText(R.string.text_no_data);
        }

    }

    public void changeLayoutGrid() {
        if (isGridAvailable) {
            recyclerView.setLayoutManager(linearLayoutManager);
            adapter = new DownloadItemAdapter(this, mediasItems);
            recyclerView.setAdapter(adapter);
            isGridAvailable = false;
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
            gridAdapter = new DownloadItemGridAdapter(this, mediasItems);
            recyclerView.setAdapter(gridAdapter);
            isGridAvailable = true;
        }
    }


    private List<MediasItem> getMediaItemsFromDBList(RealmList<Download> downloads) {
        List<MediasItem> mediasItems = new ArrayList<>();
        for (Download download : downloads) {
            mediasItems.add(download.getMediaItems());
        }
        return mediasItems;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item_clear, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_change_layout:
                if (isGridAvailable) {
                    item.setIcon(R.drawable.ic_grid_on_white_24dp);
                    changeLayoutGrid();
                } else {
                    item.setIcon(R.drawable.ic_grid_off_white_24dp);
                    changeLayoutGrid();
                }
                return true;
            case R.id.action_clear_data:
                if (gridAdapter != null && gridAdapter.getSelectedMediaItems().size() > 0) {
                    HelperUtils helperUtils = new HelperUtils(this, Constants.DOWNLOADS, gridAdapter.getSelectedMediaItems());
                    helperUtils.setOnDeleteListener(new HelperUtils.onDeleteListener() {
                        @Override
                        public void onCompleteTask(boolean status) {
                            if (status) {
                                fetchData();
                            }
                        }

                        @Override
                        public void onProgressTask(int current, int total) {

                        }
                    });
                } else {
                    HelperUtils helperUtils = new HelperUtils(this, Constants.DOWNLOADS);
                    helperUtils.setOnDeleteListener(new HelperUtils.onDeleteListener() {
                        @Override
                        public void onCompleteTask(boolean status) {
                            if (status) {
                                fetchData();
                            }
                        }

                        @Override
                        public void onProgressTask(int current, int total) {

                        }
                    });
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
//        fetchData();
    }
}

