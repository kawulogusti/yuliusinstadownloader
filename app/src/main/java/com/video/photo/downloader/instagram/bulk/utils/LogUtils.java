package com.video.photo.downloader.instagram.bulk.utils;

import android.util.Log;

import com.video.photo.downloader.instagram.bulk.BuildConfig;


public class LogUtils {
    public static void log(String x) {
        if (BuildConfig.DEBUG) {
            Log.i("InstagramDownloader", x);
        }
    }

    public static void log(int x) {
        if (BuildConfig.DEBUG) {
            Log.i("InstagramDownloader", String.valueOf(x));
        }
    }

}
