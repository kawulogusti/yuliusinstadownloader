package com.video.photo.downloader.instagram.bulk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseMedia implements Parcelable {

    public static final Creator<ResponseMedia> CREATOR = new Creator<ResponseMedia>() {
        @Override
        public ResponseMedia createFromParcel(Parcel source) {
            return new ResponseMedia(source);
        }

        @Override
        public ResponseMedia[] newArray(int size) {
            return new ResponseMedia[size];
        }
    };
    @SerializedName("medias")
    private List<MediasItem> medias;
    @SerializedName("message")
    private String message;
    @SerializedName("next")
    private String next;
    @SerializedName("status")
    private int status;

    public ResponseMedia() {
    }

    protected ResponseMedia(Parcel in) {
        this.medias = in.createTypedArrayList(MediasItem.CREATOR);
        this.message = in.readString();
        this.next = in.readString();
        this.status = in.readInt();
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public List<MediasItem> getMedias() {
        return medias;
    }

    public void setMedias(List<MediasItem> medias) {
        this.medias = medias;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "ResponseMedia{" +
                        "medias = '" + medias + '\'' +
                        ",message = '" + message + '\'' +
                        ",next = '" + next + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.medias);
        dest.writeString(this.message);
        dest.writeString(this.next);
        dest.writeInt(this.status);
    }
}