package com.video.photo.downloader.instagram.bulk.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.activity.SearchActivity;
import com.video.photo.downloader.instagram.bulk.activity.UsermediaActivity;
import com.video.photo.downloader.instagram.bulk.model.UsersItem;

import java.util.List;


/**
 * Created by ahm on 11/05/17.
 */

public class UserItemAdapter extends RecyclerView.Adapter<UserItemAdapter.MyViewHolder> {

    private Context mContext;
    private List<UsersItem> listKonten;
    private SearchActivity searchActivity;

    public UserItemAdapter(Context mContext, List<UsersItem> listKontenP) {
        this.mContext = mContext;
        this.listKonten = listKontenP;
        this.searchActivity = (SearchActivity) mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_useritem, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final UsersItem userItem = listKonten.get(position);

        Glide
                .with(mContext)
//                .load(Uri.parse(mediaItem.getImageUrl()))
                .load(userItem.getProfileImageUrlHttps())
                .error(R.drawable.ic_broken_image_black_24dp)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.imageViewThumbnail);

        holder.tv_name.setText(userItem.getName());
        holder.tv_username.setText(userItem.getUsername());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, UsermediaActivity.class);
                intent.putExtra(UsermediaActivity.USER_NAME, userItem);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listKonten.size();
    }

    /*private void showPopupMenu(UsersItem konten, View view) {
        PopupMenu popup = new PopupMenu(mContext, view);
        //inflating menu from xml resource
        popup.inflate(R.menu.song_options_menu);
        Menu menuOpts = popup.getMenu();

        Favorite favorite = DBHelper.getFavorite(konten.getMediaId());
        if (favorite != null) {
            menuOpts.getItem(0).setTitle(R.string.remove_favorite);
        } else {
            menuOpts.getItem(0).setTitle(R.string.action_favorite);
        }
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_playlist:
                        //handle menu2 click
                        searchActivity.showAddToPlaylistDialog(konten);
//                        Toast.makeText(mContext, "Coming soon..", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.action_favorite:
                        //handle menu2 click
                        DBHelper.setOrDeleteFavorite(konten);
                        if (favorite != null) {
                            Toast.makeText(mContext, "Removed from favorite", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, "Added to favorite", Toast.LENGTH_SHORT).show();
                        }
                        notifyDataSetChanged();
                        return true;

                    default:
                        return false;
                }
            }
        });
        //displaying the popup
        popup.show();
    }
*/

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewThumbnail; //, iv_download, iv_more;
        private TextView tv_name, tv_username;

        public MyViewHolder(View view) {
            super(view);
            imageViewThumbnail = view.findViewById(R.id.imageViewThumbnail);
            tv_name = view.findViewById(R.id.tv_name);
            tv_username = view.findViewById(R.id.tv_username);
        }
    }


}