package com.video.photo.downloader.instagram.bulk;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.video.photo.downloader.instagram.bulk.activity.DownloadsActivity;
import com.video.photo.downloader.instagram.bulk.activity.FavoriteActivity;
import com.video.photo.downloader.instagram.bulk.activity.HistoryActivity;
import com.video.photo.downloader.instagram.bulk.activity.SearchActivity;
import com.video.photo.downloader.instagram.bulk.activity.SettingsActivity;
import com.video.photo.downloader.instagram.bulk.activity.ShareActivity;
import com.video.photo.downloader.instagram.bulk.activity.StorydownloaderActivity;
import com.video.photo.downloader.instagram.bulk.api.ConfApp;
import com.video.photo.downloader.instagram.bulk.api.RequestAPI;
import com.video.photo.downloader.instagram.bulk.api.ResponseLogs;
import com.video.photo.downloader.instagram.bulk.service.DownloadService;
import com.video.photo.downloader.instagram.bulk.utils.AppRaterHelper;
import com.video.photo.downloader.instagram.bulk.utils.Constants;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.HelperUtils;
import com.video.photo.downloader.instagram.bulk.utils.JdkmdenJav;
import com.video.photo.downloader.instagram.bulk.utils.LogUtils;
import com.video.photo.downloader.instagram.bulk.utils.SharedPref;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements ServiceConnection, PurchasesUpdatedListener {

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private TextView tv_history, tv_favorite, tv_downloads, tv_paste, tv_upgrade, tv_story;
    private SearchView searchView;
    private DownloadService downloadService;
    private Realm realm;
    //ads
    private ConfApp confApp;
    private AdView mAdView;
    private AdRequest adRequest;
    //billings
    private BillingClient billingClient;
    private String appVersion = "emptyAppVersion";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_history = findViewById(R.id.tv_history);
        tv_favorite = findViewById(R.id.tv_favorite);
        tv_downloads = findViewById(R.id.tv_downloads);
        tv_paste = findViewById(R.id.tv_rating);
        tv_upgrade = findViewById(R.id.tv_upgrade);
        tv_story = findViewById(R.id.tv_story);
        searchView = findViewById(R.id.searchView);

        realm = Realm.getDefaultInstance();

        //package info
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (Exception ignored) {
            appVersion = "0.0";
        }

        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_home_white_24dp));
        getSupportActionBar().setTitle("Home");

        AppRaterHelper.app_launched(this);

        //billing
        setupBillings();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (HelperUtils.isValidUrl(query)) {
                    if (query.contains("instagram.com") && (query.contains("/p/") || query.contains("/tv/"))) {
                        Intent intent = new Intent(MainActivity.this, ShareActivity.class);
                        intent.putExtra(ShareActivity.SEARCH_QUERY, query);
                        startActivity(intent);
                    } else if (query.contains("instagram.com") && query.contains("/stories/")) {
                        Intent intent = new Intent(MainActivity.this, StorydownloaderActivity.class);
                        intent.putExtra(StorydownloaderActivity.POST_URL, query);
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity.this, "Insert a valid instagram post url, please.", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                    intent.putExtra(SearchActivity.SEARCH_QUERY, query);
                    startActivity(intent);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        tv_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
                startActivity(intent);
            }
        });
        tv_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FavoriteActivity.class);
                startActivity(intent);
            }
        });
        tv_downloads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DownloadsActivity.class);
                startActivity(intent);
            }
        });
        tv_paste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pasteFromClipBoard();
            }
        });

        tv_upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchRemoveAdsDialogFlow();
            }
        });

        tv_story.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, StorydownloaderActivity.class);
//                intent.putExtra(StorydownloaderActivity.POST_URL, )
                startActivity(intent);
            }
        });

        if (!checkPermission()) {
            requestPermissionAgain();
        }

        //ads
        mAdView = findViewById(R.id.adView);

        confApp = DBHelper.getConfApp();

        LogUtils.log("Conf app: " + confApp.toString());

        if (confApp != null && confApp.isPremiumUser()) {
            tv_upgrade.setText("Premium User");
        }
        //debug remove ads
        /*realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                confApp.setIsAdEnabled(false);
            }
        });*/

//        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            //Log.d("TwitterDl", "Ads is: "+confApp.isAdEnabled());
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));

            mAdView.setVisibility(View.VISIBLE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);

        } else {
            //Log.d("TwitterDl", "Ads is: "+confApp.isAdEnabled());
            mAdView.setVisibility(View.GONE);
        }


    }

    //billings
    private void setupBillings() {
        billingClient = BillingClient.newBuilder(MainActivity.this).enablePendingPurchases().setListener(this).build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
//                    Log.d("MP3Downloader", "The BillingClient is ready");
                    if (!SharedPref.getBol(Constants.PREMIUM_USER_REMOVE_ADS)) {
                        SharedPref.saveBol(Constants.PREMIUM_USER_REMOVE_ADS, true);
                        isUserAlreadyPurchased();
                    }

                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                //Log.d("MP3Downloader", "The BillingClient is not ready");
            }
        });
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> list) {

        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list != null) {
            for (Purchase purchase : list) {
                handlePurchase(purchase);
            }
        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
            // Handle an error caused by a user cancelling the purchase flow.
        } else {
            // Handle any other error codes.
        }
    }

    private void handlePurchase(Purchase purchase) {
        if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
            if (!purchase.isAcknowledged()) {
                acknowledgePurchases(purchase, 0);
            }
            // Acknowledge purchase and grant the item to the user

        } else if (purchase.getPurchaseState() == Purchase.PurchaseState.PENDING) {
            // Here you can confirm to the user that they've started the pending
            // purchase, and to complete it, they should follow instructions that
            // are given to them. You can also choose to remind the user in the
            // future to complete the purchase if you detect that it is still
            // pending.
            Toast.makeText(MainActivity.this, "The purchase is pending, please complete it to use the premium feature.", Toast.LENGTH_SHORT).show();
        }

    }

    private void acknowledgePurchases(final Purchase purchase, final int alreadyPurchased) {
        AcknowledgePurchaseParams acknowledgePurchaseParams =
                AcknowledgePurchaseParams.newBuilder()
                        .setPurchaseToken(purchase.getPurchaseToken())
                        .build();
        billingClient.acknowledgePurchase(acknowledgePurchaseParams, new AcknowledgePurchaseResponseListener() {
            @Override
            public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    if (purchase.getSku().contains(Constants.REMOVE_ADS_TEXT_ID)) {
                        saveLogsPremium(purchase);
                        performRemoveAdsRealm(alreadyPurchased);
                    }
                }
            }
        });

    }

    private void saveLogsPremium(Purchase purchase) {
//        Log.d("MP3Downloader", "Tring to send message.. ");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(JdkmdenJav.getWorker())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final RequestAPI service = retrofit.create(RequestAPI.class);

        Call<ResponseLogs> call = service.getLogsPremium(getPackageName(), appVersion,
                purchase.getOriginalJson());
        call.enqueue(new Callback<ResponseLogs>() {
            @Override
            public void onResponse(Call<ResponseLogs> call, Response<ResponseLogs> response) {

            }

            @Override
            public void onFailure(Call<ResponseLogs> call, Throwable t) {


            }
        });
    }

    private void performRemoveAdsRealm(int alreadyPurchased) {
        try {
            SharedPref.saveBol(Constants.IS_PREMIUM_USER_REMOVE_ADS, true);
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    confApp.setPremiumUser(true);
                    realm.insertOrUpdate(confApp);
                }
            });
        } finally {
            AppRaterHelper.showRestartDialog(MainActivity.this, alreadyPurchased);
        }

    }

    private void isUserAlreadyPurchased() {
        Purchase.PurchasesResult purchasesResult = billingClient.queryPurchases(BillingClient.SkuType.INAPP);
        for (Purchase purchase : purchasesResult.getPurchasesList()) {
            if (!purchase.isAcknowledged()) {
                acknowledgePurchases(purchase, 1);
            } else {
                if (purchase.getSku().contains(Constants.REMOVE_ADS_TEXT_ID)) {
                    performRemoveAdsRealm(1);
                }
            }

        }
    }

    public void launchRemoveAdsDialogFlow() {
        //Log.d("MP3Downloader", "Trying to remove ads..");
        List<String> skuList = new ArrayList<>();
        skuList.add(Constants.REMOVE_ADS_TEXT_ID);
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        billingClient.querySkuDetailsAsync(params.build(),
                new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(BillingResult billingResult,
                                                     List<SkuDetails> skuDetailsList) {
                        // Process the result.
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
                            for (SkuDetails skuDetails : skuDetailsList) {
                                String sku = skuDetails.getSku();
                                String price = skuDetails.getPrice();
                                if (Constants.REMOVE_ADS_TEXT_ID.equals(sku)) {
//                                    priceRemoveAds = price;
                                    // Retrieve a value for "skuDetails" by calling querySkuDetailsAsync().
                                    BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                                            .setSkuDetails(skuDetails)
                                            .build();
                                    billingClient.launchBillingFlow(MainActivity.this, flowParams);
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_rating:
                ratingApp();
                return true;
            case R.id.action_share:
                shareApp();
                return true;
            /*case R.id.action_settings:
                goToSettings();
                return true;*/
            case R.id.action_allow_file_permission:
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    Toast.makeText(MainActivity.this, "You already allow the file permission.", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_about:
                showAboutDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void pasteFromClipBoard() {
        ClipboardManager clipboard = (ClipboardManager)
                getSystemService(Context.CLIPBOARD_SERVICE);
        try {
            ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
            String url = item.getText().toString();
            searchView.setQuery(url, true);

        } catch (Exception e) {
            Toast.makeText(MainActivity.this, "Nothing in clipboard", Toast.LENGTH_SHORT).show();
        }
    }

    private void showAboutDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.about_text)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).show();
    }

    private void goToSettings() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    private void shareApp() {
        String urlaplikasi = "http://play.google.com/store/apps/details?id=" + getPackageName();
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_text));
        String strippedText = "Like this App? Share to your friends NOW!";
        share.putExtra(Intent.EXTRA_TEXT, "*Download this " + getResources().getString(R.string.app_name) + " App*" + "\n\n" + strippedText + "\n\nDownload at : " + urlaplikasi);
        startActivity(Intent.createChooser(share, getResources().getString(R.string.share_text)));
    }

    private void ratingApp() {
        String urlApp = "http://play.google.com/store/apps/details?id=" + getPackageName();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp));
        startActivity(browserIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, DownloadService.class);
        bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(this);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        DownloadService.MyBinder b = (DownloadService.MyBinder) binder;
        downloadService = b.getService();
//        Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        downloadService = null;
    }

    //permission
    public boolean checkPermission() {
//        //Log.d("MP3Downloader", "Requesting permission");
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;
        }
    }


    public void requestPermission() {

        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read Files");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Files");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                ActivityCompat.requestPermissions(this, permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
        }
    }

    public boolean addPermission(List<String> permissionsList, String permission) {
        if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission))
                return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            if (resultCode == RESULT_OK) {
//                runForFirstTime();
                Toast.makeText(this, "Thanks. Permission granted", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                requestPermissionAgain();
            } else {
                Toast.makeText(this, "Please allow the files permission. Main features will not working properly.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void requestPermissionAgain() {
        final android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false)
                .setTitle("Enable file permission")
                .setMessage("We need grant to write downloaded files to the storage. Enable file permission?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestPermission();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "Please allow the files permission. Main features will not working properly.", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

}
