package com.video.photo.downloader.instagram.bulk.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;

import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.video.photo.downloader.instagram.bulk.MainActivity;
import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.api.ConfApp;
import com.video.photo.downloader.instagram.bulk.api.RequestAPI;
import com.video.photo.downloader.instagram.bulk.utils.Constants;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.JdkmdenJav;
import com.video.photo.downloader.instagram.bulk.utils.LogUtils;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SplashscreenActivity extends AppCompatActivity {

    static {
        System.loadLibrary("realm-lib-sok");
    }

    static {
        System.loadLibrary("realm-lib-sok");
    }

    LottieAnimationView animationView;
    CountDownTimer waitTimer;
    private ConfApp confApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        animationView = findViewById(R.id.animation_view);
        animationView.playAnimation();
        confApp = DBHelper.getConfApp();

        //create notification channel
        createNotificationChannel();

        getAppConf();

    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void startTimer() {
        waitTimer = new CountDownTimer(200, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {

                startMainActivity();

            }
        }.start();
    }

    private void getAppConf() {
        String appVersion = "";
        String packageName = "";
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = pInfo.versionName;
            packageName = getPackageName();
        } catch (Exception ignored) {
            appVersion = "0.0";
        }
        LogUtils.log("Packagename: " + packageName + " - versionname: " + appVersion);
        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Constants.BASE_URL_DB)
//                .baseUrl(BuildConfig.BASE_URL_DB)
                .baseUrl(JdkmdenJav.getWorker())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final RequestAPI service = retrofit.create(RequestAPI.class);
        Call<ConfApp> callConfApp = service.getConfApp(packageName, appVersion);
        callConfApp.enqueue(new Callback<ConfApp>() {
            @Override
            public void onResponse(Call<ConfApp> call, Response<ConfApp> response) {
                LogUtils.log("Response: " + response.toString());
                if (response.body() != null) {
                    LogUtils.log("Success get app conf. Is not null");
                    Realm realm = null;
                    try { // I could use try-with-resources here
                        realm = Realm.getDefaultInstance();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                //Log.d("TwitterDl", "Write succesfull configuration to realm");
                                ConfApp confAppLocal = response.body();
                                if (confApp != null && confApp.isPremiumUser()) {
                                    confAppLocal.setPremiumUser(confApp.isPremiumUser());
                                }
                                realm.insertOrUpdate(confAppLocal);
                            }

                        });

                    } finally {
                        startTimer();
                    }
                } else {
                    LogUtils.log("Success get app conf. Is null");
                    Realm realm = null;
                    try { // I could use try-with-resources here
                        realm = Realm.getDefaultInstance();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                //Log.d("TwitterDl", "Write failed configuration to realm");
                                ConfApp confAppLocal = new ConfApp();
                                confAppLocal.setAppName(getResources().getString(R.string.app_name_slug));
                                confAppLocal.setIsAdEnabled(true);
                                confAppLocal.setLockPreviewThreshold(15);
                                confAppLocal.setServerUrl("http://twitterapi.com/api/");
                                confAppLocal.setVersion("1.0");
                                if (confApp != null && confApp.isPremiumUser()) {
                                    confAppLocal.setPremiumUser(confApp.isPremiumUser());
                                }
                                realm.insertOrUpdate(confAppLocal);
                            }

                        });

                    } finally {
                        startTimer();
                    }
                }
            }

            @Override
            public void onFailure(Call<ConfApp> call, Throwable t) {
                LogUtils.log("Failed to get app conf");
                Realm realm = null;
                try { // I could use try-with-resources here
                    realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            //Log.d("TwitterDl", "Write failed configuration to realm");
                            ConfApp confAppLocal = new ConfApp();
                            confAppLocal.setAppName(getResources().getString(R.string.app_name_slug));
                            confAppLocal.setIsAdEnabled(true);
                            confAppLocal.setLockPreviewThreshold(15);
                            confAppLocal.setServerUrl("http://twitterapi.com/api/");
                            confAppLocal.setVersion("1.0");
                            if (confApp != null && confApp.isPremiumUser()) {
                                confAppLocal.setPremiumUser(confApp.isPremiumUser());
                            }
                            realm.insertOrUpdate(confAppLocal);
                        }

                    });

                } finally {
                    startTimer();
                }

            }
        });


    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(Constants.CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }
    }
}
