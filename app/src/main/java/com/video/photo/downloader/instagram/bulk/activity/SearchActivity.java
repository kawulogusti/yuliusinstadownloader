package com.video.photo.downloader.instagram.bulk.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.adapter.UserItemAdapter;
import com.video.photo.downloader.instagram.bulk.api.ConfApp;
import com.video.photo.downloader.instagram.bulk.api.RequestAPI;
import com.video.photo.downloader.instagram.bulk.model.ResponseMedia;
import com.video.photo.downloader.instagram.bulk.model.ResponseUser;
import com.video.photo.downloader.instagram.bulk.utils.AppRaterHelper;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.HelperUtils;
import com.video.photo.downloader.instagram.bulk.utils.JdkmdenJav;
import com.video.photo.downloader.instagram.bulk.utils.LogUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity {

    public static final String SEARCH_QUERY = "search_query";

    static {
        System.loadLibrary("realm-lib-sok");
    }

    private ProgressBar progressBar;
    private TextView tv_status;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private UserItemAdapter adapter;


    //ads
    private ConfApp confApp;
    private AdView mAdView;
    private AdRequest adRequest;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Search results: " + getIntent().getStringExtra(SEARCH_QUERY));

        progressBar = findViewById(R.id.progress_circular);
        tv_status = findViewById(R.id.tv_status);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);
        tv_status.setText(R.string.loading);

        searchData(getIntent().getStringExtra(SEARCH_QUERY));

        //ads
        mAdView = findViewById(R.id.adView);

        confApp = DBHelper.getConfApp();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));

            mAdView.setVisibility(View.VISIBLE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);

            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.google_admob_intersial_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    requestIntersialAds();
                    if (HelperUtils.getRandomBoolean()) {
                        AppRaterHelper.showReviewDialog(SearchActivity.this);
                    }
                }
            });

        } else {
            mAdView.setVisibility(View.GONE);
        }

    }

    public void showIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            if (mInterstitialAd.isLoaded() && HelperUtils.getRandomBoolean()) {
                mInterstitialAd.show();
            } else {
                requestIntersialAds();
                if (HelperUtils.getRandomBoolean()) {
                    AppRaterHelper.showReviewDialog(SearchActivity.this);
                }
                //Log.d("TAG", "The interstitial wasn't loaded yet.");
            }
        }
    }

    public void requestIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }

    private void searchData(String query) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(JdkmdenJav.getWorker())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final RequestAPI service = retrofit.create(RequestAPI.class);
        Call<ResponseUser> responseUserCall = service.searchUsers(query);
        final Call<ResponseMedia> responseMediaCall = service.searchTweets(query);
        responseUserCall.enqueue(new Callback<ResponseUser>() {
            @Override
            public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {
                if (response.body() != null) {
                    LogUtils.log("Request response: " + response.toString());
                    LogUtils.log("Request user: " + response.body().getUsers().size());
                    if (response.body().getUsers().size() > 0) {
//                tv_status.setText("Found: "+mResponseUser.getUsers().size()+" users");
                        tv_status.setVisibility(View.GONE);
                    } else {
                        tv_status.setText(getResources().getString(R.string.no_data_user));
                    }
                    //Log.d("TwitterDl", "ResponseUser object is: "+mResponseUser.toString());
                    adapter = new UserItemAdapter(SearchActivity.this, response.body().getUsers());
                    recyclerView.setAdapter(adapter);
                    progressBar.setVisibility(View.GONE);
                } else {
                    Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ResponseUser> call, Throwable t) {
                Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
