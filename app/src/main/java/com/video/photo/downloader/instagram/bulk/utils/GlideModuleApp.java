package com.video.photo.downloader.instagram.bulk.utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public final class GlideModuleApp extends AppGlideModule {
}
