package com.video.photo.downloader.instagram.bulk.activity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.adapter.BulkDownloadItemAdapter;
import com.video.photo.downloader.instagram.bulk.api.ConfApp;
import com.video.photo.downloader.instagram.bulk.model.Download;
import com.video.photo.downloader.instagram.bulk.model.MediasItem;
import com.video.photo.downloader.instagram.bulk.utils.Constants;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.HelperUtils;
import com.video.photo.downloader.instagram.bulk.utils.SharedPref;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class BulkdownloaderActivity extends AppCompatActivity {

    public static final String MEDIA_LIST = "media_list";
    private static final String COMPLETE = "complete";
    private static final String UNCOMPLETE = "uncomplete";
    private RecyclerView recyclerView;
    private BulkDownloadItemAdapter adapter;
    private List<MediasItem> mediasItemList = new ArrayList<>();
    private List<MediasItem> mediasItemListModified = new ArrayList<>();
    private List<MediasItem> listKontenFailed = new ArrayList<>();
    private int downloadCounter = 0;
    private int mPosition = -1;
    private int mTaskJob = 0;
    private String statusDownloader = "";
    private BottomSheetDialog mBottomSheetDialog;
    private boolean isDownloadStopped = false;

    private ConfApp confApp;
    //ads

    private AdView mAdView;
    private AdRequest adRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulkdownloader);
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        mediasItemList = intent.getParcelableArrayListExtra(MEDIA_LIST);

        getSupportActionBar().setTitle("Downloader Status");
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);
        adapter = new BulkDownloadItemAdapter(this, mediasItemList);
        recyclerView.setAdapter(adapter);
        /*recyclerView.post(new Runnable() {
            @Override
            public void run() {
                // Call smooth scroll
                recyclerView.smoothScrollToPosition(adapter.getItemCount());
            }
        });*/
        runBulkDownload(mediasItemList);

        //ads
        mAdView = findViewById(R.id.adView);

        confApp = DBHelper.getConfApp();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));

            mAdView.setVisibility(View.VISIBLE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);

        } else {
            mAdView.setVisibility(View.GONE);
        }
    }

    private void showCompleteRatingDialog(Context mContext, String type) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(mContext);
        builderSingle.setCancelable(true);
        if (type.equalsIgnoreCase(COMPLETE)) {
            builderSingle.setTitle("All downloads is complete!");
            builderSingle.setMessage("Would you like to give us five starts ⭐⭐⭐⭐⭐ for our app?");

            builderSingle.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String urlApp = "http://play.google.com/store/apps/details?id=" + getPackageName();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp));
                    startActivity(browserIntent);
                    SharedPref.saveBol(Constants.RATING_OPTIONS, true);
                }
            });

            builderSingle.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        } else if (type.equalsIgnoreCase(UNCOMPLETE)) {
            builderSingle.setTitle("Download is uncomplete!");
            builderSingle.setMessage("Found " + listKontenFailed.size() + " media is failed to download. Would you like to re-download again?");

            builderSingle.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    runBulkReDownload();
                }
            });

            builderSingle.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        builderSingle.show();

    }

    public void stopDownload(MediasItem mediasItem) {
        int downloadId = mediasItemListModified.get(mediasItemList.indexOf(mediasItem)).getDownloadId();
        PRDownloader.cancel(downloadId);
        Toast.makeText(this, "Download id: " + downloadId + " canceled.", Toast.LENGTH_SHORT).show();
        adapter.notifyDataSetChanged();
    }

    private void runBulkDownload(List<MediasItem> mediasItems) {
        downloadCounter = 0;
        mTaskJob = 0;

        if (mediasItems.size() == 0) {
            Toast.makeText(this, "Nothing to download.", Toast.LENGTH_SHORT).show();
        }
        mediasItemListModified = mediasItems;

        String mBaseFolderPath = android.os.Environment
                .getExternalStorageDirectory()
                + File.separator
                + getResources().getString(R.string.app_name) + File.separator;
        if (!new File(mBaseFolderPath).exists()) {
            new File(mBaseFolderPath).mkdir();
        }


        for (MediasItem mediasItem : mediasItems) {
            mPosition = mPosition + 1;
            String file_url = mediasItem.getUrl();
            assert file_url != null;
//            String filename = new File(file_url).getName().replaceFirst("[?][^.]+$", "");
            String filename = HelperUtils.getFileNameFromPath(BulkdownloaderActivity.this, mediasItem.getUrl());
            File file = new File(mBaseFolderPath + "/" + filename);
            Download download = DBHelper.getDownload(mediasItem.getId());
            if (download == null) {
                mTaskJob = mTaskJob + 1;
                int downloadId = PRDownloader.download(mediasItem.getUrl(), mBaseFolderPath, filename)
                        .build()
                        .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                            @Override
                            public void onStartOrResume() {

                            }
                        })
                        .setOnPauseListener(new OnPauseListener() {
                            @Override
                            public void onPause() {

                            }
                        })
                        .setOnCancelListener(new OnCancelListener() {
                            @Override
                            public void onCancel() {

                            }
                        })
                        .setOnProgressListener(new OnProgressListener() {
                            @Override
                            public void onProgress(Progress progress) {

                            }
                        })
                        .start(new OnDownloadListener() {
                            @Override
                            public void onDownloadComplete() {

//                            Toast.makeText(BulkdownloaderActivity.this, "Download successful: " + filename, Toast.LENGTH_LONG).show();
                                downloadCounter = downloadCounter + 1;
                                statusDownloader = "Status: " + downloadCounter + "/" + mTaskJob;
                                assert getActionBar() != null;
                                Objects.requireNonNull(getSupportActionBar()).setTitle(statusDownloader);
                                //Log.d("TwitterDl", "Value counter: "+downloadCounter+" - mTaskJob: "+mTaskJob);
                                if (downloadCounter == mTaskJob) {
                                    if (listKontenFailed.size() <= 0) {
                                        showCompleteRatingDialog(BulkdownloaderActivity.this, COMPLETE);
                                    } else {
                                        showCompleteRatingDialog(BulkdownloaderActivity.this, UNCOMPLETE);
                                    }
                                }

                                DBHelper.setDownload(BulkdownloaderActivity.this, mediasItem);
//                            adapter.updateViewPosition(mPosition);
                                adapter.notifyDataSetChanged();
                                //scan media for new file agar bisa diindex
                                if (filename.contains(".mp4")) {
                                    MediaScannerConnection.scanFile(BulkdownloaderActivity.this,
                                            new String[]{file.getAbsolutePath()}, new String[]{"video/mp4"},
                                            new MediaScannerConnection.OnScanCompletedListener() {
                                                public void onScanCompleted(String path, Uri uri) {
                                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                                    Log.i("ExternalStorage", "-> uri=" + uri);
                                                }
                                            });

                                    ContentValues values = new ContentValues();

                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    Date date = new Date();
                                    values.put(MediaStore.Video.Media.DATE_TAKEN, dateFormat.format(date));
                                    values.put(MediaStore.Video.Media.DATE_ADDED, dateFormat.format(date));
                                    values.put(MediaStore.Video.Media.DATE_MODIFIED, dateFormat.format(date));
                                    values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
                                    values.put(MediaStore.Video.Media.DATA, file.getAbsolutePath());
                                    values.put(MediaStore.Video.Media.RELATIVE_PATH, file.getAbsolutePath());
                                    values.put(MediaStore.Video.Media.DOCUMENT_ID, mediasItem.getId());
//                                        //////Log.d("MP3Downloader", "Data Download = "+file.getAbsolutePath());

                                    getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
                                } else {
                                    MediaScannerConnection.scanFile(BulkdownloaderActivity.this,
                                            new String[]{file.getAbsolutePath()}, new String[]{"image/jpeg", "image/png"},
                                            new MediaScannerConnection.OnScanCompletedListener() {
                                                public void onScanCompleted(String path, Uri uri) {
                                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                                    Log.i("ExternalStorage", "-> uri=" + uri);
                                                }
                                            });

                                    ContentValues values = new ContentValues();
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    Date date = new Date();
                                    values.put(MediaStore.Images.Media.DATE_TAKEN, dateFormat.format(date));
                                    values.put(MediaStore.Images.Media.DATE_ADDED, dateFormat.format(date));
                                    values.put(MediaStore.Images.Media.DATE_MODIFIED, dateFormat.format(date));
                                    if (filename.contains(".jpg")) {
                                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                                    } else if (filename.contains(".png")) {
                                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
                                    }

                                    values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
                                    values.put(MediaStore.Images.Media.RELATIVE_PATH, file.getAbsolutePath());
                                    values.put(MediaStore.Images.Media.DOCUMENT_ID, mediasItem.getId());
//                                        //////Log.d("MP3Downloader", "Data Download = "+file.getAbsolutePath());

                                    getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                                }


                            }

                            @Override
                            public void onError(Error error) {
                                listKontenFailed.add(mediasItem);
                                downloadCounter = downloadCounter + 1;
                                if (downloadCounter == mTaskJob && listKontenFailed.size() > 0) {
                                    showCompleteRatingDialog(BulkdownloaderActivity.this, UNCOMPLETE);
                                }
                                Toast.makeText(BulkdownloaderActivity.this, "Failed to download: " + filename + ". Server message: " + error.getServerErrorMessage(), Toast.LENGTH_SHORT).show();
                            }

                        });
                mediasItemListModified.get(mediasItemListModified.indexOf(mediasItem)).setDownloadId(downloadId);
            }

        }
    }

    private void runBulkReDownload() {

        if (listKontenFailed.size() == 0) {
            Toast.makeText(this, "Nothing to download.", Toast.LENGTH_SHORT).show();
        }

        String mBaseFolderPath = android.os.Environment
                .getExternalStorageDirectory()
                + File.separator
                + getResources().getString(R.string.app_name) + File.separator;
        if (!new File(mBaseFolderPath).exists()) {
            new File(mBaseFolderPath).mkdir();
        }


        for (MediasItem mediasItem : listKontenFailed) {
            String file_url = mediasItem.getUrl();
            assert file_url != null;
            String filename = new File(file_url).getName().replaceFirst("[?][^.]+$", "");
            File file = new File(mBaseFolderPath + "/" + filename);
            Download download = DBHelper.getDownload(mediasItem.getId());
            if (download == null) {
                int downloadId = PRDownloader.download(mediasItem.getUrl(), mBaseFolderPath, filename)
                        .build()
                        .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                            @Override
                            public void onStartOrResume() {

                            }
                        })
                        .setOnPauseListener(new OnPauseListener() {
                            @Override
                            public void onPause() {

                            }
                        })
                        .setOnCancelListener(new OnCancelListener() {
                            @Override
                            public void onCancel() {

                            }
                        })
                        .setOnProgressListener(new OnProgressListener() {
                            @Override
                            public void onProgress(Progress progress) {

                            }
                        })
                        .start(new OnDownloadListener() {
                            @Override
                            public void onDownloadComplete() {

//                            Toast.makeText(BulkdownloaderActivity.this, "Download successful: " + filename, Toast.LENGTH_LONG).show();
                                listKontenFailed.remove(mediasItem);
                                DBHelper.setDownload(BulkdownloaderActivity.this, mediasItem);
//                            adapter.updateViewPosition(mPosition);
                                adapter.notifyDataSetChanged();
                                //scan media for new file agar bisa diindex
                                if (filename.contains(".mp4")) {
                                    MediaScannerConnection.scanFile(BulkdownloaderActivity.this,
                                            new String[]{file.getAbsolutePath()}, new String[]{"video/mp4"},
                                            new MediaScannerConnection.OnScanCompletedListener() {
                                                public void onScanCompleted(String path, Uri uri) {
                                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                                    Log.i("ExternalStorage", "-> uri=" + uri);
                                                }
                                            });

                                    ContentValues values = new ContentValues();

                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    Date date = new Date();
                                    values.put(MediaStore.Video.Media.DATE_TAKEN, dateFormat.format(date));
                                    values.put(MediaStore.Video.Media.DATE_ADDED, dateFormat.format(date));
                                    values.put(MediaStore.Video.Media.DATE_MODIFIED, dateFormat.format(date));
                                    values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
                                    values.put(MediaStore.Video.Media.DATA, file.getAbsolutePath());
                                    values.put(MediaStore.Video.Media.RELATIVE_PATH, file.getAbsolutePath());
                                    values.put(MediaStore.Video.Media.DOCUMENT_ID, mediasItem.getId());
//                                        //////Log.d("MP3Downloader", "Data Download = "+file.getAbsolutePath());

                                    getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
                                } else {
                                    MediaScannerConnection.scanFile(BulkdownloaderActivity.this,
                                            new String[]{file.getAbsolutePath()}, new String[]{"image/jpeg", "image/png"},
                                            new MediaScannerConnection.OnScanCompletedListener() {
                                                public void onScanCompleted(String path, Uri uri) {
                                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                                    Log.i("ExternalStorage", "-> uri=" + uri);
                                                }
                                            });

                                    ContentValues values = new ContentValues();
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    Date date = new Date();
                                    values.put(MediaStore.Images.Media.DATE_TAKEN, dateFormat.format(date));
                                    values.put(MediaStore.Images.Media.DATE_ADDED, dateFormat.format(date));
                                    values.put(MediaStore.Images.Media.DATE_MODIFIED, dateFormat.format(date));
                                    if (filename.contains(".jpg")) {
                                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                                    } else if (filename.contains(".png")) {
                                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
                                    }

                                    values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
                                    values.put(MediaStore.Images.Media.RELATIVE_PATH, file.getAbsolutePath());
                                    values.put(MediaStore.Images.Media.DOCUMENT_ID, mediasItem.getId());
//                                        //////Log.d("MP3Downloader", "Data Download = "+file.getAbsolutePath());

                                    getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                                }


                            }

                            @Override
                            public void onError(Error error) {
                                Toast.makeText(BulkdownloaderActivity.this, "Failed to re-download: " + filename + ". Server message: " + error.getServerErrorMessage(), Toast.LENGTH_SHORT).show();
                            }

                        });
            }

        }
    }

    private void showStatusDialogBottomSheet() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.layout_status_downloader, null);
        mBottomSheetDialog.setContentView(sheetView);
        Button btn_redownload = sheetView.findViewById(R.id.btn_redownload);
        TextView tv_status = sheetView.findViewById(R.id.tv_status);
        String status = "Download success: " + downloadCounter + "/" + mTaskJob + "\nDownload failed: " + listKontenFailed.size();
        tv_status.setText(status);
        btn_redownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runBulkReDownload();
            }
        });

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                // Do something
            }
        });

        mBottomSheetDialog.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item_bulk_downloader, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_stop_all:
                if (isDownloadStopped()) {
                    runBulkDownload(mediasItemList);
                    item.setTitle("Stop All");
//                    item.setIcon(getResources().getDrawable(R.drawable.ic_stop_white_24dp));
                    setDownloadStopped(false);
                    adapter.notifyDataSetChanged();
                    Toast.makeText(this, "Bulk download running..", Toast.LENGTH_SHORT).show();
                } else {
                    PRDownloader.cancelAll();
                    item.setTitle("Start All");
//                    item.setIcon(getResources().getDrawable(R.drawable.ic_bulk_download_white_24dp));
                    setDownloadStopped(true);
                    adapter.notifyDataSetChanged();
                    Toast.makeText(this, "All downloads was stopped.", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_info:
                showStatusDialogBottomSheet();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        PRDownloader.cancelAll();
        return true;
    }

    public boolean isDownloadStopped() {
        return isDownloadStopped;
    }

    public void setDownloadStopped(boolean downloadStopped) {
        isDownloadStopped = downloadStopped;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
        PRDownloader.cancelAll();
    }
}
