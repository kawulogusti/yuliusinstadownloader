package com.video.photo.downloader.instagram.bulk.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.api.ConfApp;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;

import io.realm.Realm;

public class SettingsActivity extends AppCompatActivity {

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        realm = Realm.getDefaultInstance();
        assert getSupportActionBar() != null;   //null check
        ConfApp confApp = DBHelper.getConfApp();
        if (confApp != null && confApp.isPremiumUser()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Settings");
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Upgrade to Premium Account");
        }


    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
