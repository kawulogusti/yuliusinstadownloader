package com.video.photo.downloader.instagram.bulk.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.video.photo.downloader.instagram.bulk.MainActivity;
import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.adapter.TweetItemAdapter;
import com.video.photo.downloader.instagram.bulk.api.ConfApp;
import com.video.photo.downloader.instagram.bulk.api.RequestAPI;
import com.video.photo.downloader.instagram.bulk.model.ResponseMedia;
import com.video.photo.downloader.instagram.bulk.service.DownloadService;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.JdkmdenJav;
import com.video.photo.downloader.instagram.bulk.utils.LogUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ShareActivity extends AppCompatActivity implements ServiceConnection {

    public static final String SEARCH_QUERY = "search_query";
    private static final Pattern urlPattern = Pattern.compile(
            "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                    + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    static {
        System.loadLibrary("realm-lib-sok");
    }

    private DownloadService downloadService;
    private TextView tv_status;
    private RecyclerView recyclerView;
    private TweetItemAdapter adapter;
    private ProgressBar progressBar;
    private boolean isServiceConnected = false;
    private String receivedUrl, receivedUrlQuery;
    //ads
    private ConfApp confApp;
    private AdView mAdView;
    private AdRequest adRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Post Media");

        tv_status = findViewById(R.id.tv_status);
        recyclerView = findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progress_circular);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);
        tv_status.setText(R.string.loading);

        Intent receiverdIntent = getIntent();

        receivedUrlQuery = receiverdIntent.getStringExtra(SEARCH_QUERY);
        if (receivedUrlQuery != null) {
            performActionFromSearchIntent(receivedUrlQuery);
        }

        String receivedAction = receiverdIntent.getAction();
        String receivedType = receiverdIntent.getType();
        receivedUrl = receiverdIntent.getStringExtra(Intent.EXTRA_TEXT);

        if (receivedAction != null && receivedType != null && receivedUrl != null) {
            performActionFromShareIntent(receivedAction, receivedType, receivedUrl);
        }

        //ads
        mAdView = findViewById(R.id.adView);

        confApp = DBHelper.getConfApp();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));

            mAdView.setVisibility(View.VISIBLE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);

        } else {
            mAdView.setVisibility(View.GONE);
        }

    }

    private void performActionFromSearchIntent(String receivedUrl) {
        String path = "";
        try {
            path = new URL(receivedUrl).getPath();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String id = "";
        if (receivedUrl.contains("/p/")) {
            id = path.split("p/")[1];
        } else if (receivedUrl.contains("/tv/")) {
            id = path.split("tv/")[1];
        }
        id = id.replace("/", "");

        LogUtils.log("Url: " + receivedUrl + " - path: " + path + " - id: " + id);
        getTweetMedia(id);
    }

    private void performActionFromShareIntent(String receivedAction, String receivedType, String receivedUrl) {
        Matcher matcher = urlPattern.matcher(receivedUrl);
        while (matcher.find()) {
            int matchStart = matcher.start(1);
            int matchEnd = matcher.end();
            receivedUrl = receivedUrl.substring(matchStart, matchEnd);
            // now you have the offsets of a URL match
        }
        LogUtils.log("Url: " + receivedUrl);
        assert receivedUrl != null;
        String path = "";
        try {
            path = new URL(receivedUrl).getPath();
            LogUtils.log("path: " + path);
        } catch (MalformedURLException e) {
            LogUtils.log("error: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        String id = path.split("p/")[1];
        id = id.replace("/", "");
        //Log.d("TwitterDl", "Url: "+receivedUrl+" - path: "+path+" - id: "+id);

        if (receivedAction != null && receivedAction.equals(Intent.ACTION_SEND)) {
            // check mime type
            if (receivedType != null && receivedType.startsWith("text/")) {
                getTweetMedia(id);
            }
        }
    }

    private void getTweetMedia(String id) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(JdkmdenJav.getWorker())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final RequestAPI service = retrofit.create(RequestAPI.class);
        Call<ResponseMedia> responseMediaCall = service.getTweetMedia(id);
        responseMediaCall.enqueue(new Callback<ResponseMedia>() {
            @Override
            public void onResponse(Call<ResponseMedia> call, Response<ResponseMedia> response) {
                if (response.body() != null) {
                    //Log.d("TwitterDl", "Request tweet: "+response.body().toString());
                    if (response.body().getMedias().size() > 0) {
                        assert getSupportActionBar() != null;
                        if (response.body().getMedias().size() > 0) {
                            getSupportActionBar().setTitle(response.body().getMedias().get(0).getUsername() + " Tweet");
                        } else {
                            getSupportActionBar().setTitle("Post Media");
                        }

                        tv_status.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        adapter = new TweetItemAdapter(ShareActivity.this, response.body().getMedias());
                        recyclerView.setAdapter(adapter);

                    } else {
                        tv_status.setText(getResources().getString(R.string.no_data_tweet));
                        progressBar.setVisibility(View.GONE);

                    }
                } else {
                    Toast.makeText(ShareActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseMedia> call, Throwable t) {
                Toast.makeText(ShareActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, DownloadService.class);
        bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(this);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        DownloadService.MyBinder b = (DownloadService.MyBinder) binder;
        downloadService = b.getService();
        isServiceConnected = true;
//        Toast.makeText(ShareActivity.this, "Connected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        downloadService = null;
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        if (receivedUrlQuery == null) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        if (receivedUrlQuery == null) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        super.onBackPressed();
    }
}
