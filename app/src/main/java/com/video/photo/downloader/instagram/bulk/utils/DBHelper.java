package com.video.photo.downloader.instagram.bulk.utils;

import android.content.Context;

import com.video.photo.downloader.instagram.bulk.api.ConfApp;
import com.video.photo.downloader.instagram.bulk.model.Download;
import com.video.photo.downloader.instagram.bulk.model.Favorite;
import com.video.photo.downloader.instagram.bulk.model.History;
import com.video.photo.downloader.instagram.bulk.model.MediasItem;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

public class DBHelper {

    public final static String TWEET_ID = "id";
    private static final String RECENTLY_VIEWED = "recentlyViewed";

    public static ConfApp getConfApp() {

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            ConfApp confApp = realm.where(ConfApp.class).findFirst();
            if (confApp != null) {
                return confApp;
            } else {
                return null;
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static Favorite getFavorite(String id) {

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            Favorite favorite = realm.where(Favorite.class).contains(TWEET_ID, id).findFirst();
            if (favorite != null) {
                return favorite;
            } else {
                return null;
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static Download getDownload(String id) {

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            Download download = realm.where(Download.class).contains(TWEET_ID, id).findFirst();
            if (download != null) {
                return download;
            } else {
                return null;
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static History getHistory(String id) {

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            History history = realm.where(History.class).contains(TWEET_ID, id).findFirst();
            if (history != null) {
                return history;
            } else {
                return null;
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void setOrDeleteFavorite(MediasItem mediasItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            Favorite favorite = realm.where(Favorite.class).contains(TWEET_ID, mediasItem.getId()).findFirst();
            if (favorite != null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        favorite.deleteFromRealm();
                    }
                });
            } else {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        //Log.d("TwitterDl", "Write favorite item");
                        realm.copyToRealmOrUpdate(mediasItem.getFavoriteItem());
                    }
                });
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void deleteFavorite(MediasItem mediasItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            Favorite favorite = realm.where(Favorite.class).contains(TWEET_ID, mediasItem.getId()).findFirst();
            if (favorite != null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        favorite.deleteFromRealm();
                    }
                });
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }


    public static void deleteDownload(MediasItem mediasItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            Download download = realm.where(Download.class).contains(TWEET_ID, mediasItem.getId()).findFirst();
            if (download != null) {
                LogUtils.log("Downloads is not null. " + mediasItem.getId());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        download.deleteFromRealm();
                    }
                });
            } else {
                LogUtils.log("Downloads i null. " + mediasItem.getId());
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void deleteAllDownloads() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.where(Download.class).findAll().deleteAllFromRealm();
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void deleteAllHistory() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.where(History.class).findAll().deleteAllFromRealm();
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void deleteAllFavorites() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.where(Favorite.class).findAll().deleteAllFromRealm();
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void setOrDeleteHistory(MediasItem mediasItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            History history = realm.where(History.class).contains(TWEET_ID, mediasItem.getId()).findFirst();
            if (history != null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        history.deleteFromRealm();
                    }
                });
            } else {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        //Log.d("TwitterDl", "Write history item");
                        realm.copyToRealmOrUpdate(mediasItem.getHistoryItem());
                    }
                });
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void deleteHistory(MediasItem mediasItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            History history = realm.where(History.class).contains(TWEET_ID, mediasItem.getId()).findFirst();
            if (history != null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        history.deleteFromRealm();
                    }
                });
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void setHistory(MediasItem mediasItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    //Log.d("TwitterDl", "Write history item");
                    realm.copyToRealmOrUpdate(mediasItem.getHistoryItem());
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void setDownload(Context context, MediasItem mediasItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    //Log.d("TwitterDl", "Write download item");
                    realm.copyToRealmOrUpdate(mediasItem.getDownloadItem(context));
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static RealmList<History> getHistoryList() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            RealmResults<History> item = realm.where(History.class).findAll().sort(RECENTLY_VIEWED, Sort.DESCENDING);
            RealmList<History> trackItems = new RealmList<History>();
            trackItems.addAll(item.subList(0, item.size()));
            return trackItems;

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static RealmList<Favorite> getFavoriteList() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            RealmResults<Favorite> item = realm.where(Favorite.class).findAll().sort(RECENTLY_VIEWED, Sort.DESCENDING);
            RealmList<Favorite> trackItems = new RealmList<Favorite>();
            trackItems.addAll(item.subList(0, item.size()));
            return trackItems;

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static RealmList<Download> getDownloadList() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            RealmResults<Download> item = realm.where(Download.class).findAll().sort(RECENTLY_VIEWED, Sort.DESCENDING);
            RealmList<Download> trackItems = new RealmList<Download>();
            trackItems.addAll(item.subList(0, item.size()));
            return trackItems;

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }
}
