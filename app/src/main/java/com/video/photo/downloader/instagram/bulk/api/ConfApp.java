package com.video.photo.downloader.instagram.bulk.api;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ConfApp extends RealmObject {

    @PrimaryKey
    @SerializedName("appName")
    private String appName;

    @SerializedName("serverUrl")
    private String serverUrl;

    @SerializedName("isAdEnabled")
    private boolean isAdEnabled;

    @SerializedName("version")
    private String version;

    @SerializedName("lockPreviewThreshold")
    private int lockPreviewThreshold;

    private boolean isPremiumUser;

    public boolean isPremiumUser() {
        return isPremiumUser;
    }

    public void setPremiumUser(boolean premiumUser) {
        isPremiumUser = premiumUser;
    }

    public int getLockPreviewThreshold() {
        return lockPreviewThreshold;
    }

    public void setLockPreviewThreshold(int lockPreviewThreshold) {
        this.lockPreviewThreshold = lockPreviewThreshold;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public void setIsAdEnabled(boolean isAdEnabled) {
        this.isAdEnabled = isAdEnabled;
    }

    public boolean isAdEnabled() {
        return isAdEnabled;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return
                "ConfApp{" +
                        "app_name = '" + appName + '\'' +
                        ",serverUrl = '" + serverUrl + '\'' +
                        ",isAdEnabled = '" + isAdEnabled + '\'' +
                        ",version = '" + version + '\'' +
                        ",isPremiumUser = '" + isPremiumUser + '\'' +
                        ",lockPreviewThreshold = '" + lockPreviewThreshold + '\'' +
                        "}";
    }
}
