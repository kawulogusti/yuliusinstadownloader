package com.video.photo.downloader.instagram.bulk.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.adapter.HistoryItemAdapter;
import com.video.photo.downloader.instagram.bulk.adapter.HistoryItemGridAdapter;
import com.video.photo.downloader.instagram.bulk.api.ConfApp;
import com.video.photo.downloader.instagram.bulk.model.History;
import com.video.photo.downloader.instagram.bulk.model.MediasItem;
import com.video.photo.downloader.instagram.bulk.utils.AppRaterHelper;
import com.video.photo.downloader.instagram.bulk.utils.Constants;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.HelperUtils;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

public class HistoryActivity extends AppCompatActivity implements RewardedVideoAdListener {

    private TextView tv_status;
    private RecyclerView recyclerView;
    private HistoryItemAdapter adapter;
    private HistoryItemGridAdapter gridAdapter;
    private ProgressBar progressBar;
    private List<MediasItem> mediasItems = new ArrayList<>();

    private ConfApp confApp;
    //ads

    private AdView mAdView;
    private AdRequest adRequest;
    private InterstitialAd mInterstitialAd;
    private RewardedVideoAd mRewardedVideoAd;

    private LinearLayoutManager linearLayoutManager;
    private GridLayoutManager gridLayoutManager;
    private boolean isGridAvailable = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("History");

        tv_status = findViewById(R.id.tv_status);
        recyclerView = findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progress_circular);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);
        tv_status.setText(R.string.loading);
        fetchData();

        //ads
        mAdView = findViewById(R.id.adView);

        confApp = DBHelper.getConfApp();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));

            mAdView.setVisibility(View.VISIBLE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);

            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.google_admob_intersial_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    requestIntersialAds();
                    if (HelperUtils.getRandomBoolean()) {
                        AppRaterHelper.showReviewDialog(HistoryActivity.this);
                    }
                }
            });

            mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
            mRewardedVideoAd.setRewardedVideoAdListener(this);

            loadRewardedVideoAd();


        } else {
            mAdView.setVisibility(View.GONE);
        }
    }

    public void showIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            if (mInterstitialAd.isLoaded() && HelperUtils.getRandomBoolean()) {
                mInterstitialAd.show();
            } else {
                requestIntersialAds();
                if (HelperUtils.getRandomBoolean()) {
                    AppRaterHelper.showReviewDialog(HistoryActivity.this);
                }
                //Log.d("TAG", "The interstitial wasn't loaded yet.");
            }
        }
    }

    public void showIntersialAdsWithoutRandom() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else {
                requestIntersialAds();
                //Log.d("TAG", "The interstitial wasn't loaded yet.");
            }
        }
    }

    public void requestIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }

    public void fetchData() {
        mediasItems = getMediaItemsFromDBList(DBHelper.getHistoryList());
        gridAdapter = new HistoryItemGridAdapter(this, mediasItems);
        recyclerView.setAdapter(gridAdapter);
        if (mediasItems.size() > 0) {
            progressBar.setVisibility(View.GONE);
            tv_status.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            tv_status.setVisibility(View.VISIBLE);
            tv_status.setText(R.string.text_no_data);
        }
    }

    public void changeLayoutGrid() {
        if (isGridAvailable) {
            recyclerView.setLayoutManager(linearLayoutManager);
            adapter = new HistoryItemAdapter(this, mediasItems);
            recyclerView.setAdapter(adapter);
            isGridAvailable = false;
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
            gridAdapter = new HistoryItemGridAdapter(this, mediasItems);
            recyclerView.setAdapter(gridAdapter);
            isGridAvailable = true;
        }
    }

    private List<MediasItem> getMediaItemsFromDBList(RealmList<History> historyList) {
        List<MediasItem> mediasItems = new ArrayList<>();
        for (History history : historyList) {
            mediasItems.add(history.getMediaItems());
        }
        return mediasItems;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item_clear_and_bulk_download, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_change_layout:
                if (isGridAvailable) {
                    item.setIcon(R.drawable.ic_grid_on_white_24dp);
                    changeLayoutGrid();
                } else {
                    item.setIcon(R.drawable.ic_grid_off_white_24dp);
                    changeLayoutGrid();
                }
                return true;
            case R.id.action_clear_data:
                if (gridAdapter != null && gridAdapter.getSelectedMediaItems().size() > 0) {
                    HelperUtils helperUtils = new HelperUtils(this, Constants.HISTORY, gridAdapter.getSelectedMediaItems());
                    helperUtils.setOnDeleteListener(new HelperUtils.onDeleteListener() {
                        @Override
                        public void onCompleteTask(boolean status) {
                            if (status) {
                                fetchData();
                            }
                        }

                        @Override
                        public void onProgressTask(int current, int total) {

                        }
                    });
                } else {
                    HelperUtils helperUtils = new HelperUtils(this, Constants.HISTORY);
                    helperUtils.setOnDeleteListener(new HelperUtils.onDeleteListener() {
                        @Override
                        public void onCompleteTask(boolean status) {
                            if (status) {
                                fetchData();
                            }
                        }

                        @Override
                        public void onProgressTask(int current, int total) {

                        }
                    });
                }

                return true;
            case R.id.action_select_all:
                if (gridAdapter != null) {
                    if (gridAdapter.isSelectedAllEnabled()) {
                        gridAdapter.setSelectedAllEnabled(false);
                    } else {
                        gridAdapter.setSelectedAllEnabled(true);
                    }
                    gridAdapter.notifyDataSetChanged();
                }
                return true;
            case R.id.action_run_bulk_download:
                if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
                    showRewardActionDialog(HistoryActivity.this);
                } else {
                    displayVideoRewardedAds();
                }

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mRewardedVideoAd.pause(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mRewardedVideoAd.destroy(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mRewardedVideoAd.resume(this);
        }
    }

    private void loadRewardedVideoAd() {
        mRewardedVideoAd.loadAd(getString(R.string.google_admob_video_id),
                new AdRequest.Builder().build());
    }

    @Override
    public void onRewarded(RewardItem reward) {
        //Toast.makeText(this, "onRewarded! currency: " + reward.getType() + "  amount: " +
        //        reward.getAmount(), Toast.LENGTH_SHORT).show();
        if (gridAdapter != null && gridAdapter.getSelectedMediaItems().size() > 0) {
            Intent intent = new Intent(HistoryActivity.this, BulkdownloaderActivity.class);
            intent.putParcelableArrayListExtra(BulkdownloaderActivity.MEDIA_LIST, gridAdapter.getSelectedMediaItems());
            startActivity(intent);
        } else {
            Toast.makeText(this, "Select media at the first, please. Then, run again.", Toast.LENGTH_SHORT).show();
        }
        // Reward the user.
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        //Toast.makeText(this, "onRewardedVideoAdLeftApplication",
        //        Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdClosed() {
        //Toast.makeText(this, "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();
        loadRewardedVideoAd();
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int errorCode) {
        //Toast.makeText(this, "onRewardedVideoAdFailedToLoad", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        //Toast.makeText(this, "onRewardedVideoAdLoaded", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdOpened() {
        //Toast.makeText(this, "onRewardedVideoAdOpened", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoStarted() {
        //Toast.makeText(this, "onRewardedVideoStarted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoCompleted() {
        //Toast.makeText(this, "onRewardedVideoCompleted", Toast.LENGTH_SHORT).show();
    }

    public void displayVideoRewardedAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            if (mRewardedVideoAd.isLoaded()) {
                mRewardedVideoAd.show();
            } else {
                Toast.makeText(this, "Video ads is unavailable. Make sure you have a good internet connection!", Toast.LENGTH_SHORT).show();
                if (gridAdapter != null && gridAdapter.getSelectedMediaItems().size() > 0) {
                    Intent intent = new Intent(HistoryActivity.this, BulkdownloaderActivity.class);
                    intent.putParcelableArrayListExtra(BulkdownloaderActivity.MEDIA_LIST, gridAdapter.getSelectedMediaItems());
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Select media at the first, please. Then, run again.", Toast.LENGTH_SHORT).show();
                }

            }
        } else {
            if (gridAdapter != null && gridAdapter.getSelectedMediaItems().size() > 0) {
                Intent intent = new Intent(HistoryActivity.this, BulkdownloaderActivity.class);
                intent.putParcelableArrayListExtra(BulkdownloaderActivity.MEDIA_LIST, gridAdapter.getSelectedMediaItems());
                startActivity(intent);
            } else {
                Toast.makeText(this, "Select media at the first, please. Then, run again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showRewardActionDialog(Context mContext) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(mContext);
        builderSingle.setCancelable(true);
        builderSingle.setTitle("Watch Video Ads");
        builderSingle.setMessage("Would you like to watch video ads to continue perform bulk download operation?");

        builderSingle.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                displayVideoRewardedAds();
            }
        });

        builderSingle.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builderSingle.show();

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

}
