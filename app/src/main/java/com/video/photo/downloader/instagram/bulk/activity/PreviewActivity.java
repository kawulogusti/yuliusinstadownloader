package com.video.photo.downloader.instagram.bulk.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.api.ConfApp;
import com.video.photo.downloader.instagram.bulk.model.Download;
import com.video.photo.downloader.instagram.bulk.model.Favorite;
import com.video.photo.downloader.instagram.bulk.model.MediasItem;
import com.video.photo.downloader.instagram.bulk.service.DownloadService;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.GlideApp;
import com.video.photo.downloader.instagram.bulk.utils.HelperUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PreviewActivity extends AppCompatActivity implements View.OnTouchListener, RewardedVideoAdListener {

    public static final String CURRENT_MEDIA_ITEM = "current_media_item";
    public static final String MEDIA_LIST = "list_media";
    public static final String CURRENT_POSITION = "current_position";
    // We can be in one of these 3 states
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    private static final int PREVIEW_COUNTER = 15;
    private static final String PLAYBACK_TIME = "play_time";
    public final String ABSOLUTE_PATH = "absolute_path";
    public final String ABSOLUTE_PATH_NOTIF = "absolute_path_notif";
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    // These matrices will be used to move and zoom image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();
    int mode = NONE;
    // Remember some things for zooming
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;
    String savedItemClicked;
    private VideoView videoView;
    private ImageView imageView;
    //ads
    private View layout_view_anchor;
    private String filePath;
    private String filePathNotif;
    private int mCurrentPosition = 0;
    private int mCurrentPositionIndex = 0;
    private Button buttonDownload, buttonPrev, buttonNext;
    private int postion;
    private LinearLayout linearLayout_layout_anchor;
    private MediasItem currentMediasItem;
    private ArrayList<MediasItem> mediasItemList = new ArrayList<>();
    private ConfApp confApp;
    private AdView mAdView;
    private AdRequest adRequest;
    private RewardedVideoAd mRewardedVideoAd;
    private boolean isPreviewLocked = false;
    private int counterPreview = 0;
    private MenuInflater inflater;
    private Menu mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //ads
        mAdView = findViewById(R.id.adView);
        layout_view_anchor = findViewById(R.id.layout_view_anchor);
        linearLayout_layout_anchor = findViewById(R.id.layout_control);

        confApp = DBHelper.getConfApp();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));

            mAdView.setVisibility(View.VISIBLE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);

            mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
            mRewardedVideoAd.setRewardedVideoAdListener(this);

            loadRewardedVideoAd();


        } else {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) linearLayout_layout_anchor.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            linearLayout_layout_anchor.setLayoutParams(lp);
            mAdView.setVisibility(View.GONE);
        }

        videoView = findViewById(R.id.videoView);
        buttonDownload = findViewById(R.id.buttonDownload);
        buttonPrev = findViewById(R.id.buttonPrev);
        buttonNext = findViewById(R.id.buttonNext);
        imageView = findViewById(R.id.imageView);
        imageView.setOnTouchListener(this);

        Intent intent = getIntent();
        currentMediasItem = intent.getParcelableExtra(CURRENT_MEDIA_ITEM);
        mediasItemList = intent.getParcelableArrayListExtra(MEDIA_LIST);
        mCurrentPositionIndex = intent.getIntExtra(CURRENT_POSITION, 0);

        if (currentMediasItem != null) {
            Download download = DBHelper.getDownload(currentMediasItem.getId());
            if (download != null) {

            }
            filePath = currentMediasItem.getUrl();
            DBHelper.setHistory(currentMediasItem);
            //Log.d("TwitterDl", "Filepath current: " + filePath);
        }

        if (intent.getStringExtra(ABSOLUTE_PATH) != null) {
            filePath = intent.getStringExtra(ABSOLUTE_PATH);
        }

        if (intent.getStringExtra(ABSOLUTE_PATH_NOTIF) != null) {
            filePathNotif = intent.getStringExtra(ABSOLUTE_PATH_NOTIF);
        }


        if (filePath != null) {
            String filename = new File(filePath).getName().replaceFirst("[?][^.]+$", "");
            getSupportActionBar().setTitle(filename);
            //Log.d("TwitterDl", "Nilai path download = " + filePath);
            loadMedia(currentMediasItem, savedInstanceState);

            if (mediasItemList == null) {
                buttonNext.setVisibility(View.GONE);
                buttonPrev.setVisibility(View.GONE);
            } else {
                buttonNext.setVisibility(View.VISIBLE);
                buttonPrev.setVisibility(View.VISIBLE);
            }

            postion = mCurrentPositionIndex;
//            //Log.d("TwitterDl", "MediaItems is: " + currentMediasItem.toString());
//            //Log.d("TwitterDl", "Position is: " + postion + " - size: " + mediasItemList.size());

            buttonNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    postion = postion + 1;
                    if (postion < 0 || postion >= mediasItemList.size()) {
                        Toast.makeText(getBaseContext(), "End of list", Toast.LENGTH_SHORT).show();
                        postion = postion - 1;
                        filePath = mediasItemList.get(postion).getUrl();
                        currentMediasItem = mediasItemList.get(postion);
                        DBHelper.setHistory(currentMediasItem);
                        loadMedia(currentMediasItem, savedInstanceState);
                        Favorite favorite = DBHelper.getFavorite(currentMediasItem.getId());
                        if (favorite == null) {
                            mMenu.getItem(1).setIcon(R.drawable.ic_favorite_border_white_24dp);
                        } else {
                            mMenu.getItem(1).setIcon(R.drawable.ic_favorite_white_24dp);
                        }
                    } else {
                        filePath = mediasItemList.get(postion).getUrl();
                        currentMediasItem = mediasItemList.get(postion);
                        DBHelper.setHistory(currentMediasItem);
                        loadMedia(currentMediasItem, savedInstanceState);
                        Favorite favorite = DBHelper.getFavorite(currentMediasItem.getId());
                        if (favorite == null) {
                            mMenu.getItem(1).setIcon(R.drawable.ic_favorite_border_white_24dp);
                        } else {
                            mMenu.getItem(1).setIcon(R.drawable.ic_favorite_white_24dp);
                        }
                    }
                }
            });

            buttonPrev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (postion != -1) {
                        postion = postion - 1;
                    }
                    if (postion < 0 || postion >= mediasItemList.size()) {
                        Toast.makeText(getBaseContext(), "End of list", Toast.LENGTH_SHORT).show();
                        postion = postion + 1;
                        filePath = mediasItemList.get(postion).getUrl();
                        currentMediasItem = mediasItemList.get(postion);
                        DBHelper.setHistory(currentMediasItem);
                        loadMedia(currentMediasItem, savedInstanceState);
                        Favorite favorite = DBHelper.getFavorite(currentMediasItem.getId());
                        if (favorite == null) {
                            mMenu.getItem(1).setIcon(R.drawable.ic_favorite_border_white_24dp);
                        } else {
                            mMenu.getItem(1).setIcon(R.drawable.ic_favorite_white_24dp);
                        }
                    } else {
                        filePath = mediasItemList.get(postion).getUrl();
                        currentMediasItem = mediasItemList.get(postion);
                        DBHelper.setHistory(currentMediasItem);
                        loadMedia(currentMediasItem, savedInstanceState);
                        Favorite favorite = DBHelper.getFavorite(currentMediasItem.getId());
                        if (favorite == null) {
                            mMenu.getItem(1).setIcon(R.drawable.ic_favorite_border_white_24dp);
                        } else {
                            mMenu.getItem(1).setIcon(R.drawable.ic_favorite_white_24dp);
                        }
                    }

                }
            });

            File file = new File(currentMediasItem.getDownloadItem(this).getFilePath());
            if (file.isFile()) {
                filePath = currentMediasItem.getDownloadItem(this).getFilePath();
                buttonDownload.setText(R.string.text_share);
                buttonDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shareFile(PreviewActivity.this, filePath);
                    }
                });
            } else {
                filePath = currentMediasItem.getUrl();
                buttonDownload.setText(R.string.text_download);
                buttonDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        downloadFile(currentMediasItem);
                    }
                });
            }


        } /*else if (filePathNotif != null) {
            loadMedia(filePathNotif, savedInstanceState);
            //Log.d("VideoDownload", "Nilai path notif= " + filePathNotif);
            buttonDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MediasItem mediasItem = new MediasItem();
                    mediasItem.setUrl(filePathNotif);
                    downloadFile(mediasItem);
                }
            });
            buttonNext.setVisibility(View.GONE);
            buttonPrev.setVisibility(View.GONE);
        }*/

        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void loadMedia(MediasItem mediasItem, Bundle savedInstanceState) {
        counterPreview = counterPreview + 1;
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            if (counterPreview % confApp.getLockPreviewThreshold() == 0) {
                isPreviewLocked = true;
            }
        }

        if (!isPreviewLocked) {
            File file = new File(mediasItem.getDownloadItem(this).getFilePath());
            if (file.isFile()) {
                filePath = mediasItem.getDownloadItem(this).getFilePath();
                buttonDownload.setText(R.string.text_share);
                buttonDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shareFile(PreviewActivity.this, filePath);
                    }
                });
            } else {
                filePath = mediasItem.getUrl();
                buttonDownload.setText(R.string.text_download);
                buttonDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        downloadFile(mediasItem);
                    }
                });
            }
            //Log.d("TwitterDl", "Filepath is: " + filePath);
            String filename = new File(filePath).getName().replaceFirst("[?][^.]+$", "");
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle(filename);
            //Log.d("VideoDownload", "Filepath = " + filePath);
            if (filePath.contains(".jpg") || filePath.contains(".png")) {
                imageView.setVisibility(View.VISIBLE);
                videoView.setVisibility(View.GONE);
                GlideApp
                        .with(this)
                        .load(filePath)// can also be a drawable
                        .error(R.drawable.ic_broken_image_black_24dp) // will be displayed if the image cannot be loaded
                        .fallback(R.drawable.ic_broken_image_black_24dp)
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(imageView);
            } else if (filePath.contains(".mp4") || filePath.contains(".mp3")) {
                if (filePath.contains("http")) {
                    Toast.makeText(this, "Please wait.. Buffering..", Toast.LENGTH_SHORT).show();
                }
                videoView.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);
                Uri uri = Uri.parse(filePath);
                videoView.setVideoURI(uri);
                videoView.requestFocus();
                if (mCurrentPosition > 0) {
                    videoView.seekTo(mCurrentPosition);
                } else {
                    // Skipping to 1 shows the first frame of the video.
                    videoView.seekTo(1);
                }
                videoView.start();

                if (savedInstanceState != null) {
                    mCurrentPosition = savedInstanceState.getInt(PLAYBACK_TIME);
                }

                MediaController controller = new MediaController(this);
                controller.setMediaPlayer(videoView);
                videoView.setMediaController(controller);
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        controller.setAnchorView(layout_view_anchor);
                    }
                });

                videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        // Implementation here.
                        Toast.makeText(PreviewActivity.this, "Done",
                                Toast.LENGTH_SHORT).show();
                        videoView.seekTo(1);
                    }
                });
            }
        } else {
            showRewardesAdsDialog();
        }

    }

    private void showRewardesAdsDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PreviewActivity.this);
        builder.setMessage(R.string.text_watch_ads_confirmation_to_continue);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                displayVideoRewardedAds();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // TODO Auto-generated method stub

        ImageView view = (ImageView) v;
        dumpEvent(event);

        // Handle touch events here...
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
//                Log.d(TAG, "mode=DRAG");
                mode = DRAG;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                oldDist = spacing(event);
//                Log.d(TAG, "oldDist=" + oldDist);
                if (oldDist > 10f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode = ZOOM;
//                    Log.d(TAG, "mode=ZOOM");
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
//                Log.d(TAG, "mode=NONE");
                break;
            case MotionEvent.ACTION_MOVE:
                if (mode == DRAG) {
                    // ...
                    matrix.set(savedMatrix);
                    matrix.postTranslate(event.getX() - start.x, event.getY()
                            - start.y);
                } else if (mode == ZOOM) {
                    float newDist = spacing(event);
//                    Log.d(TAG, "newDist=" + newDist);
                    if (newDist > 10f) {
                        matrix.set(savedMatrix);
                        float scale = newDist / oldDist;
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    }
                }
                break;
        }

        view.setImageMatrix(matrix);
        return true;
    }

    private void dumpEvent(MotionEvent event) {
        String names[] = {"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE",
                "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"};
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);
        if (actionCode == MotionEvent.ACTION_POINTER_DOWN
                || actionCode == MotionEvent.ACTION_POINTER_UP) {
            sb.append("(pid ").append(
                    action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
            sb.append(")");
        }
        sb.append("[");
        for (int i = 0; i < event.getPointerCount(); i++) {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())
                sb.append(";");
        }
        sb.append("]");
//        Log.d(TAG, sb.toString());
    }

    /**
     * Determine the space between the first two fingers
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    /**
     * Calculate the mid point of the first two fingers
     */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    private void downloadFile(MediasItem mediasItem) {
        if (!checkPermission()) {
            requestPermissionAgain();
        } else {
            //Log.d("TwitterDl", "Performing download on: " + mediasItem.getUrl());
            Intent intent = new Intent(DownloadService.BROADCAST_START_DOWNLOAD_FILE);
            intent.putExtra(DownloadService.FILE_URL, mediasItem);
            sendBroadcast(intent);
        }
        /*//Log.d("VideoDownload", filePath);
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri videoUri = Uri.parse(filePath);
        if (filePath.endsWith(".jpg")) {
            sharingIntent.setType("image/jpg");
        } else if (filePath.endsWith(".mp4")){
            sharingIntent.setType("video/mp4");
        } else {
            sharingIntent.setType("image/*");
        }

        sharingIntent.putExtra(Intent.EXTRA_STREAM, videoUri);
        startActivity(Intent.createChooser(sharingIntent, "Share media using.."));*/


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(PLAYBACK_TIME, videoView.getCurrentPosition());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        inflater = getMenuInflater();
        mMenu = menu;
        inflater.inflate(R.menu.menu_item_tweet, menu);
        Favorite favorite = DBHelper.getFavorite(currentMediasItem.getId());
        if (favorite == null) {
            mMenu.getItem(1).setIcon(R.drawable.ic_favorite_border_white_24dp);
        } else {
            mMenu.getItem(1).setIcon(R.drawable.ic_favorite_white_24dp);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_show_caption:
                if (currentMediasItem != null) {
                    showCaptionDialog(currentMediasItem.getText());
                } else {
                    Toast.makeText(this, "No caption found.", Toast.LENGTH_SHORT).show();
                }

                return true;
            case R.id.action_favorite:
                DBHelper.setOrDeleteFavorite(currentMediasItem);
                Favorite favorite = DBHelper.getFavorite(currentMediasItem.getId());
                if (favorite == null) {
                    Toast.makeText(this, "Removed from favorite", Toast.LENGTH_SHORT).show();
                    item.setIcon(R.drawable.ic_favorite_border_white_24dp);
                } else {
                    Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show();
                    item.setIcon(R.drawable.ic_favorite_white_24dp);
                }

                return true;
            case R.id.action_share:
                Download download = DBHelper.getDownload(currentMediasItem.getId());
                if (download != null) {
                    shareFile(this, HelperUtils.getFilePath(this, currentMediasItem.getUrl()));
                } else {
                    Toast.makeText(this, "Please download first.", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_open_twitter:
                lauchTwitter(this, currentMediasItem);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean isIntentAvailable(Context ctx, Intent intent) {
        final PackageManager packageManager = ctx.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    private void lauchTwitter(Context context, MediasItem mediasItem) {

        Uri uri = Uri.parse("http://instagram.com/p/" + mediasItem.getExpandedUrl());
        Intent insta = new Intent(Intent.ACTION_VIEW, uri);
        insta.setPackage("com.instagram.android");

        if (isIntentAvailable(context, insta)) {
            context.startActivity(insta);
        } else {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/p/" + mediasItem.getExpandedUrl())));
        }
        /*Intent intent = null;
        try {
            // get the Twitter app if possible
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
//            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=USERID"));
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://status?status_id=" + mediasItem.getId()));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } catch (Exception e) {
            // no Twitter app, revert to browser
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mediasItem.getExpandedUrl()));
        }
        context.startActivity(intent);*/
    }

    private void shareFile(Context context, String filePath) {
//        //Log.d("VideoDownload", filePath);
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri videoUri = Uri.parse(filePath);
        if (filePath.endsWith(".jpg")) {
            sharingIntent.setType("image/jpg");
        } else if (filePath.endsWith(".png")) {
            sharingIntent.setType("image/png");
        }
        if (filePath.endsWith(".mp4")) {
            sharingIntent.setType("video/mp4");
        } else {
            sharingIntent.setType("image/*");
        }

        sharingIntent.putExtra(Intent.EXTRA_STREAM, videoUri);
        context.startActivity(Intent.createChooser(sharingIntent, "Share media using.."));
    }

    private void showCaptionDialog(String text) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(text)
                .setPositiveButton("Copy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        copyCaption(text);
                    }
                }).show();
    }

    private void copyCaption(String text) {
        ClipboardManager clipboard = (ClipboardManager)
                getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("text", text);
        assert clipboard != null;
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this, "Caption copied", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mRewardedVideoAd.pause(this);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            videoView.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mRewardedVideoAd.destroy(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mRewardedVideoAd.resume(this);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        videoView.stopPlayback();
        this.currentMediasItem = null;
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.currentMediasItem = null;
        finish();
        return true;
    }

    private void loadRewardedVideoAd() {
        mRewardedVideoAd.loadAd(getString(R.string.google_admob_video_id),
                new AdRequest.Builder().build());
    }

    @Override
    public void onRewarded(RewardItem reward) {
        //Toast.makeText(this, "onRewarded! currency: " + reward.getType() + "  amount: " +
        //        reward.getAmount(), Toast.LENGTH_SHORT).show();
        isPreviewLocked = false;
        Toast.makeText(this, "The limit is unlocked! You are enable to continue previewing media.", Toast.LENGTH_SHORT).show();

        // Reward the user.
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        //Toast.makeText(this, "onRewardedVideoAdLeftApplication",
        //        Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdClosed() {
        //Toast.makeText(this, "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();
        loadRewardedVideoAd();
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int errorCode) {
        //Toast.makeText(this, "onRewardedVideoAdFailedToLoad", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        //Toast.makeText(this, "onRewardedVideoAdLoaded", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdOpened() {
        //Toast.makeText(this, "onRewardedVideoAdOpened", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoStarted() {
        //Toast.makeText(this, "onRewardedVideoStarted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoCompleted() {
        //Toast.makeText(this, "onRewardedVideoCompleted", Toast.LENGTH_SHORT).show();
    }

    public void displayVideoRewardedAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            if (mRewardedVideoAd.isLoaded()) {
                mRewardedVideoAd.show();
            }
        }
    }

    //permission
    public boolean checkPermission() {
//        //Log.d("MP3Downloader", "Requesting permission");
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;
        }
    }

    public void requestPermission() {

        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read Files");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Files");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                ActivityCompat.requestPermissions(this, permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
        }
    }

    public boolean addPermission(List<String> permissionsList, String permission) {
        if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission))
                return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            if (resultCode == RESULT_OK) {
//                runForFirstTime();
                Toast.makeText(this, "Thanks. Permission granted", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                requestPermissionAgain();
            } else {
                Toast.makeText(this, "Please allow the files permission", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void requestPermissionAgain() {
        final android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false)
                .setTitle("Enable file permission")
                .setMessage("We need grant to write downloaded files to the storage. Enable file permission?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestPermission();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(PreviewActivity.this, "Please allow the files permission. Main features will not working properly.", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }
}
