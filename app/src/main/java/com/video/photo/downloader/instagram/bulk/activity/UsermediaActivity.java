package com.video.photo.downloader.instagram.bulk.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.adapter.UserMediaItemAdapter;
import com.video.photo.downloader.instagram.bulk.api.ConfApp;
import com.video.photo.downloader.instagram.bulk.api.RequestAPI;
import com.video.photo.downloader.instagram.bulk.model.MediasItem;
import com.video.photo.downloader.instagram.bulk.model.ResponseMedia;
import com.video.photo.downloader.instagram.bulk.model.UsersItem;
import com.video.photo.downloader.instagram.bulk.utils.AppRaterHelper;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.GlideApp;
import com.video.photo.downloader.instagram.bulk.utils.HelperUtils;
import com.video.photo.downloader.instagram.bulk.utils.JdkmdenJav;
import com.video.photo.downloader.instagram.bulk.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UsermediaActivity extends AppCompatActivity implements RewardedVideoAdListener {

    public static final String USER_NAME = "username";

    static {
        System.loadLibrary("realm-lib-sok");
    }

    private RecyclerView recyclerView;
    private TextView tv_status;
    private UserMediaItemAdapter adapter;
    private ImageView imageViewThumbnail; //iv_download, iv_more;
    private TextView tv_name, tv_username;
    private UsersItem usersItem;
    private int page = 1;
    private boolean isFirstTimeRequest = true;
    private boolean isContentFullyLoaded = false;
    private List<MediasItem> mediasItemList = new ArrayList<>();
    private ProgressBar progressBar, progressBarMain;
    //ads
    private ConfApp confApp;
    private AdView mAdView;
    private AdRequest adRequest;
    private InterstitialAd mInterstitialAd;
    private RewardedVideoAd mRewardedVideoAd;

    //app
    private String nextToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usermedia);

        usersItem = getIntent().getParcelableExtra(USER_NAME);

        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(usersItem.getUsername() + " Profile");

        //ads
        mAdView = findViewById(R.id.adView);

        confApp = DBHelper.getConfApp();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));

            mAdView.setVisibility(View.VISIBLE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);

            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.google_admob_intersial_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    requestIntersialAds();
                    if (HelperUtils.getRandomBoolean()) {
                        AppRaterHelper.showReviewDialog(UsermediaActivity.this);
                    }

                }
            });

            mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
            mRewardedVideoAd.setRewardedVideoAdListener(this);

            loadRewardedVideoAd();

        } else {
            mAdView.setVisibility(View.GONE);
        }

        progressBar = findViewById(R.id.progress_circular);
        progressBarMain = findViewById(R.id.progress_circular_main);
        tv_status = findViewById(R.id.tv_status);
        recyclerView = findViewById(R.id.recyclerView);
        imageViewThumbnail = findViewById(R.id.imageViewThumbnail);
//        iv_download = findViewById(R.id.iv_download);
//        iv_more = findViewById(R.id.iv_more);
        tv_name = findViewById(R.id.tv_name);
        tv_username = findViewById(R.id.tv_username);


        tv_status.setText(getResources().getString(R.string.loading));

        GlideApp
                .with(this)
                .load(usersItem.getProfileImageUrlHttps())
                .error(R.drawable.ic_person_black_24dp)
                .apply(RequestOptions.circleCropTransform())
                .into(imageViewThumbnail);
        tv_username.setText(usersItem.getUsername());
        tv_name.setText(usersItem.getName());

        recyclerView.setHasFixedSize(true);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);
        getUserMedia(usersItem.getIdStr(), nextToken, isFirstTimeRequest);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {
                    progressBar.setVisibility(View.VISIBLE);
                    if (isContentFullyLoaded) {
                        page = page + 1;
                        isContentFullyLoaded = false;
                        isFirstTimeRequest = false;
                        getUserMedia(usersItem.getIdStr(), nextToken, isFirstTimeRequest);
                    } else {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        });

        /*iv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showIntersialAds();
                if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
                    showRewardActionDialog(UsermediaActivity.this);
                } else {
                    Intent intent = new Intent(UsermediaActivity.this, BulkdownloaderActivity.class);
                    intent.putParcelableArrayListExtra(BulkdownloaderActivity.MEDIA_LIST, adapter.getSelectedMediaItems());
                    startActivity(intent);
                }

            }
        });

        iv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(v);
            }
        });*/

    }

    public void showIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            if (mInterstitialAd.isLoaded() && HelperUtils.getRandomBoolean()) {
                mInterstitialAd.show();
            } else {
                requestIntersialAds();
                if (HelperUtils.getRandomBoolean()) {
                    AppRaterHelper.showReviewDialog(UsermediaActivity.this);
                }

                //Log.d("TAG", "The interstitial wasn't loaded yet.");
            }
        }
    }

    public void requestIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mRewardedVideoAd.pause(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mRewardedVideoAd.destroy(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            mRewardedVideoAd.resume(this);
        }
    }

    private void loadRewardedVideoAd() {
        mRewardedVideoAd.loadAd(getString(R.string.google_admob_video_id),
                new AdRequest.Builder().build());
    }

    @Override
    public void onRewarded(RewardItem reward) {
//        Toast.makeText(this, "onRewarded! currency: " + reward.getType() + "  amount: " +
//                reward.getAmount(), Toast.LENGTH_SHORT).show();
        if (adapter != null && adapter.getSelectedMediaItems().size() > 0) {
            Intent intent = new Intent(UsermediaActivity.this, BulkdownloaderActivity.class);
            intent.putParcelableArrayListExtra(BulkdownloaderActivity.MEDIA_LIST, adapter.getSelectedMediaItems());
            startActivity(intent);
        } else {
            Toast.makeText(this, "Select media at the first, please. Then, run again.", Toast.LENGTH_SHORT).show();
        }

        // Reward the user.
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        //Toast.makeText(this, "onRewardedVideoAdLeftApplication",
        //        Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdClosed() {
        //Toast.makeText(this, "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();
        loadRewardedVideoAd();
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int errorCode) {
        //Toast.makeText(this, "onRewardedVideoAdFailedToLoad", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        //Toast.makeText(this, "onRewardedVideoAdLoaded", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdOpened() {
        //Toast.makeText(this, "onRewardedVideoAdOpened", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoStarted() {
        //Toast.makeText(this, "onRewardedVideoStarted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoCompleted() {
        //Toast.makeText(this, "onRewardedVideoCompleted", Toast.LENGTH_SHORT).show();
    }

    public void displayVideoRewardedAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            if (mRewardedVideoAd.isLoaded()) {
                mRewardedVideoAd.show();
            } else {
                Toast.makeText(this, "Video ads is unavailable. Make sure you have a good internet connection!", Toast.LENGTH_SHORT).show();
                if (adapter != null && adapter.getSelectedMediaItems().size() > 0) {
                    Intent intent = new Intent(UsermediaActivity.this, BulkdownloaderActivity.class);
                    intent.putParcelableArrayListExtra(BulkdownloaderActivity.MEDIA_LIST, adapter.getSelectedMediaItems());
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Select media at the first, please. Then, run again.", Toast.LENGTH_SHORT).show();
                }

            }
        } else {
            if (adapter != null && adapter.getSelectedMediaItems().size() > 0) {
                Intent intent = new Intent(UsermediaActivity.this, BulkdownloaderActivity.class);
                intent.putParcelableArrayListExtra(BulkdownloaderActivity.MEDIA_LIST, adapter.getSelectedMediaItems());
                startActivity(intent);
            } else {
                Toast.makeText(this, "Select media at the first, please. Then, run again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*private void showPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(UsermediaActivity.this, view);
        //inflating menu from xml resource
        popup.inflate(R.menu.menu_item_usermedia);
        Menu menuOpts = popup.getMenu();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_open_twitter:
//                        Toast.makeText(UsermediaActivity.this, "Open twitter action..", Toast.LENGTH_SHORT).show();
                        lauchTwitter(UsermediaActivity.this, usersItem.getUsername());
                        return true;

                    default:
                        return false;
                }
            }
        });
        //displaying the popup
        popup.show();
    }*/

    private void showRewardActionDialog(Context mContext) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(mContext);
        builderSingle.setCancelable(true);
        builderSingle.setTitle("Watch Video Ads");
        builderSingle.setMessage("Would you like to watch video ads to continue perform bulk download operation?");

        builderSingle.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                displayVideoRewardedAds();
            }
        });

        builderSingle.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builderSingle.show();

    }

    private boolean isIntentAvailable(Context ctx, Intent intent) {
        final PackageManager packageManager = ctx.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    private void lauchTwitter(Context context, String username) {

        Uri uri = Uri.parse("http://instagram.com/_u/" + username);
        Intent insta = new Intent(Intent.ACTION_VIEW, uri);
        insta.setPackage("com.instagram.android");

        if (isIntentAvailable(context, insta)) {
            context.startActivity(insta);
        } else {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/" + username)));
        }
    }

    private void getUserMedia(String query, String next, final boolean isFirstTimeRequest) {
        //Log.d("TwitterDl", "Value page: " + page + " - isFirstTimeRequest: " + isFirstTimeRequest);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(JdkmdenJav.getWorker())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final RequestAPI service = retrofit.create(RequestAPI.class);
        Call<ResponseMedia> responseMediaCall = service.getUserMedia(query, next);
        responseMediaCall.enqueue(new Callback<ResponseMedia>() {
            @Override
            public void onResponse(Call<ResponseMedia> call, Response<ResponseMedia> response) {
                LogUtils.log("response: " + response.toString());
                if (response.body() != null) {
                    nextToken = response.body().getNext();
                    //Log.d("TwitterDl", "Request tweet: " + response.body().toString());
                    if (response.body().getMedias().size() > 0) {
                        tv_status.setVisibility(View.GONE);
                        if (isFirstTimeRequest) {
                            progressBarMain.setVisibility(View.GONE);
                            mediasItemList.addAll(response.body().getMedias());
                            adapter = new UserMediaItemAdapter(UsermediaActivity.this, mediasItemList);
                            recyclerView.setAdapter(adapter);
                            if (response.body().getMedias().size() < 12) {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(UsermediaActivity.this, "Scroll down to get more tweets.", Toast.LENGTH_SHORT).show();
                            } else {
                                progressBar.setVisibility(View.GONE);
                            }
                        } else {
                            mediasItemList.addAll(response.body().getMedias());
                            progressBar.setVisibility(View.GONE);
                        }

                        isContentFullyLoaded = true;

                    } else {
                        if (isFirstTimeRequest) {
                            tv_status.setText(getResources().getString(R.string.no_data_tweet));
                            progressBarMain.setVisibility(View.GONE);
                        } else {
                            progressBar.setVisibility(View.GONE);
                        }

                    }
                } else {
                    Toast.makeText(UsermediaActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseMedia> call, Throwable t) {
                Toast.makeText(UsermediaActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item_usermedia, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_select_all:
                if (adapter != null) {
                    if (adapter.isSelectedAllEnabled()) {
                        adapter.setSelectedAllEnabled(false);
                    } else {
                        adapter.setSelectedAllEnabled(true);
                    }
                    adapter.notifyDataSetChanged();
                }
                return true;
            case R.id.action_download_all:
                if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
                    showRewardActionDialog(UsermediaActivity.this);
                } else {
                    displayVideoRewardedAds();
                }
                return true;
            case R.id.action_open_twitter:
                lauchTwitter(UsermediaActivity.this, usersItem.getUsername());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (adapter != null && adapter.isSelectedAllEnabled()) {
            adapter.setSelectedAllEnabled(false);
            adapter.notifyDataSetChanged();
        } else {
            finish();
        }
    }
}
