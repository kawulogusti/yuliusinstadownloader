package com.video.photo.downloader.instagram.bulk.service;

import android.Manifest;
import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.model.MediasItem;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.HelperUtils;
import com.video.photo.downloader.instagram.bulk.utils.LogUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.video.photo.downloader.instagram.bulk.utils.Constants.CHANNEL_ID;

public class DownloadService extends Service {

    public static String BROADCAST_START_DOWNLOAD_FILE = "start_download_file_instagram";
    public static int NOTIFICATION_ID = 56784;
    public static String FILE_URL = "file_url_instagram";
    private final IBinder mBinder = new MyBinder();
    private NotificationCompat.Builder builder;
    private NotificationManagerCompat notificationManager;
    private MediasItem currentMediasItem;
    private BroadcastReceiver startDownloadFile = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!checkPermission()) {
                Toast.makeText(context, "Please allow the files permission. Download operation require file permission.", Toast.LENGTH_SHORT).show();
            } else {
                MediasItem mediasItem = intent.getParcelableExtra(FILE_URL);
                currentMediasItem = mediasItem;
                assert mediasItem != null;
                String file_url = currentMediasItem.getUrl();
                if (HelperUtils.isValidUrl(file_url)) {
                    String mBaseFolderPath = android.os.Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + getResources().getString(R.string.app_name) + File.separator;
                    if (!new File(mBaseFolderPath).exists()) {
                        new File(mBaseFolderPath).mkdir();
                    }
                    assert file_url != null;
                    String filename = new File(file_url).getName().split("\\?")[0];
                    LogUtils.log("Filename is: " + filename);
//                    filename = getResources().getString(R.string.app_name_slug)+"_"+mediasItem.getUsername()+"_"+filename;
                    File file = new File(mBaseFolderPath + "/" + filename);
                    if (!file.exists()) {
                        try {
                            addNotificationProgress(currentMediasItem);

                            int downloadId = PRDownloader.download(file_url, mBaseFolderPath, filename)
                                    .build()
                                    .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                                        @Override
                                        public void onStartOrResume() {

                                        }
                                    })
                                    .setOnPauseListener(new OnPauseListener() {
                                        @Override
                                        public void onPause() {

                                        }
                                    })
                                    .setOnCancelListener(new OnCancelListener() {
                                        @Override
                                        public void onCancel() {

                                        }
                                    })
                                    .setOnProgressListener(new OnProgressListener() {
                                        @Override
                                        public void onProgress(Progress progress) {
                                            if (builder != null) {
                                                builder.setProgress((int) progress.totalBytes, (int) progress.currentBytes, false);
                                                builder.setSmallIcon(R.drawable.ic_stat_name);
                                                notificationManager.notify(NOTIFICATION_ID, builder.build());
//                                            //Log.d("TwitterDl", "Max: "+(int) progress.totalBytes+" - current: "+(int) progress.currentBytes);
                                            }


                                        }
                                    })
                                    .start(new OnDownloadListener() {
                                        @Override
                                        public void onDownloadComplete() {
                                            Toast.makeText(DownloadService.this, "Download successful", Toast.LENGTH_LONG).show();

                                            builder.setContentTitle("Download successfull");
                                            builder.setSmallIcon(R.drawable.ic_stat_name);
                                            builder.setContentText(filename.length() > 21 ? filename.substring(0, 20) : filename);
                                            notificationManager.notify(NOTIFICATION_ID, builder.build());

                                            DBHelper.setDownload(DownloadService.this, currentMediasItem);


                                            //scan media for new file agar bisa diindex
                                            if (filename.contains(".mp4")) {
                                                MediaScannerConnection.scanFile(DownloadService.this,
                                                        new String[]{file.getAbsolutePath()}, new String[]{"video/mp4"},
                                                        new MediaScannerConnection.OnScanCompletedListener() {
                                                            public void onScanCompleted(String path, Uri uri) {
                                                                Log.i("ExternalStorage", "Scanned " + path + ":");
                                                                Log.i("ExternalStorage", "-> uri=" + uri);
                                                            }
                                                        });

                                                ContentValues values = new ContentValues();

                                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                Date date = new Date();
                                                values.put(MediaStore.Video.Media.DATE_TAKEN, dateFormat.format(date));
                                                values.put(MediaStore.Video.Media.DATE_ADDED, dateFormat.format(date));
                                                values.put(MediaStore.Video.Media.DATE_MODIFIED, dateFormat.format(date));
                                                values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
                                                values.put(MediaStore.Video.Media.DATA, file.getAbsolutePath());
                                                values.put(MediaStore.Video.Media.RELATIVE_PATH, file.getAbsolutePath());
                                                values.put(MediaStore.Video.Media.DOCUMENT_ID, mediasItem.getId());
//                                        //////Log.d("MP3Downloader", "Data Download = "+file.getAbsolutePath());

                                                getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
                                            } else {
                                                MediaScannerConnection.scanFile(DownloadService.this,
                                                        new String[]{file.getAbsolutePath()}, new String[]{"image/jpeg", "image/png"},
                                                        new MediaScannerConnection.OnScanCompletedListener() {
                                                            public void onScanCompleted(String path, Uri uri) {
                                                                Log.i("ExternalStorage", "Scanned " + path + ":");
                                                                Log.i("ExternalStorage", "-> uri=" + uri);
                                                            }
                                                        });

                                                ContentValues values = new ContentValues();
                                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                Date date = new Date();
                                                values.put(MediaStore.Images.Media.DATE_TAKEN, dateFormat.format(date));
                                                values.put(MediaStore.Images.Media.DATE_ADDED, dateFormat.format(date));
                                                values.put(MediaStore.Images.Media.DATE_MODIFIED, dateFormat.format(date));
                                                if (filename.contains(".jpg")) {
                                                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                                                } else if (filename.contains(".png")) {
                                                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
                                                }

                                                values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
                                                values.put(MediaStore.Images.Media.RELATIVE_PATH, file.getAbsolutePath());
                                                values.put(MediaStore.Images.Media.DOCUMENT_ID, mediasItem.getId());
//                                        //////Log.d("MP3Downloader", "Data Download = "+file.getAbsolutePath());

                                                getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                                            }


                                        }

                                        @Override
                                        public void onError(Error error) {
                                            Toast.makeText(DownloadService.this, "Error. Failed to download." + error.getServerErrorMessage()+" : "+error.getConnectionException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                            builder.setContentTitle("Download failed");
                                            builder.setSmallIcon(R.drawable.ic_stat_name);
                                            builder.setContentText(filename.length() > 21 ? filename.substring(0, 20) : filename);
                                            notificationManager.notify(NOTIFICATION_ID, builder.build());
                                        }

                                    });


                            Toast.makeText(DownloadService.this, "Download is starting.. See the progress on notification bar.", Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
//                        //Log.d("VideoDownload", "Error = "+e.getLocalizedMessage());
                            Toast.makeText(DownloadService.this, "Nothing to download. = " + e.getLocalizedMessage() + ".\n Try again later, please!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(DownloadService.this, filename + " - File already downloaded.", Toast.LENGTH_LONG).show();
//                    //Log.d("VideoDownload", "File arleady exists");
                    }
                } else {
                    Toast.makeText(DownloadService.this, "Not valid URL.", Toast.LENGTH_LONG).show();
                }
            }

        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {

        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        register_startDownloadFile();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(startDownloadFile);
    }

    public boolean checkPermission() {
//        //Log.d("MP3Downloader", "Requesting permission");
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;
        }
    }

    private void addNotificationProgress(MediasItem mediasItem) {
        String filename = new File(mediasItem.getUrl()).getName().replaceFirst("[?][^.]+$", "");
        builder = new NotificationCompat.Builder(DownloadService.this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Downloading")
                .setContentText(filename)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setDefaults(Notification.DEFAULT_LIGHTS)
//                .setContentIntent(resultPendingIntent)
                .setOnlyAlertOnce(true)
                .setAutoCancel(true)
                .setVibrate(new long[]{0L});

        notificationManager = NotificationManagerCompat.from(DownloadService.this);

        // notificationId is a unique int for each notification that you must define
        //Log.d("TwitterDl", "Notif id: "+mediasItem.getNotificationId());
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    private void register_startDownloadFile() {
        //Register startdownloadfile receiver
        IntentFilter filter = new IntentFilter(BROADCAST_START_DOWNLOAD_FILE);
        registerReceiver(startDownloadFile, filter);
    }

    public class MyBinder extends Binder {
        public DownloadService getService() {
            return DownloadService.this;
        }
    }
}
