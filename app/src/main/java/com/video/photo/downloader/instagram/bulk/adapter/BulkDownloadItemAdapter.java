package com.video.photo.downloader.instagram.bulk.adapter;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.activity.BulkdownloaderActivity;
import com.video.photo.downloader.instagram.bulk.activity.PreviewActivity;
import com.video.photo.downloader.instagram.bulk.model.Download;
import com.video.photo.downloader.instagram.bulk.model.MediasItem;
import com.video.photo.downloader.instagram.bulk.utils.Constants;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.SharedPref;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by ahm on 11/05/17.
 */

public class BulkDownloadItemAdapter extends RecyclerView.Adapter<BulkDownloadItemAdapter.MyViewHolder> {

    private static final String COMPLETE = "complete";
    private static final String UNCOMPLETE = "uncomplete";
    private Context mContext;
    private List<MediasItem> listKonten;
    private List<MediasItem> listKontenFailed = new ArrayList<>();
    private BulkdownloaderActivity bulkdownloaderActivity;
    private int downloadCounter = 0;

    public BulkDownloadItemAdapter(Context mContext, List<MediasItem> listKontenP) {
        this.mContext = mContext;
        this.listKonten = listKontenP;
        this.bulkdownloaderActivity = (BulkdownloaderActivity) mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_downloaditem, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final MediasItem mediasItem = listKonten.get(position);

        String file_url = mediasItem.getUrl();
        assert file_url != null;
        String filename = new File(file_url).getName().replaceFirst("[?][^.]+$", "");

        Glide
                .with(mContext)
//                .load(Uri.parse(mediaItem.getImageUrl()))
                .load(mediasItem.getUrl())
                .error(R.drawable.ic_broken_image_black_24dp)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.imageViewThumbnail);

        holder.tv_name.setText(mediasItem.getName());
        holder.tv_filename.setText(filename);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, PreviewActivity.class);
                intent.putExtra(PreviewActivity.CURRENT_MEDIA_ITEM, mediasItem);
                intent.putParcelableArrayListExtra(PreviewActivity.MEDIA_LIST, new ArrayList<MediasItem>(listKonten));
                intent.putExtra(PreviewActivity.CURRENT_POSITION, position);
                mContext.startActivity(intent);
            }
        });

        Download download = DBHelper.getDownload(mediasItem.getId());
        if (download == null) {
//        if (!file.exists()) {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.iv_button_stop.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_stop_black_24dp));
            holder.iv_button_stop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bulkdownloaderActivity.stopDownload(mediasItem);
                    holder.iv_button_stop.setVisibility(View.INVISIBLE);
                    holder.progressBar.setIndeterminate(false);
                    holder.progressBar.setMax(100);
                    holder.progressBar.setProgress(0);
                    holder.tv_filename.setText(filename + " Canceled");
                }
            });

            if (bulkdownloaderActivity.isDownloadStopped()) {
                holder.iv_button_stop.setVisibility(View.INVISIBLE);
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.progressBar.setIndeterminate(false);
                holder.progressBar.setMax(100);
                holder.progressBar.setProgress(0);
                holder.tv_filename.setText(filename + " Canceled");
            }
        } else {
            holder.progressBar.setVisibility(View.GONE);
            holder.iv_button_stop.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_offline_pin_black_24dp));
            holder.iv_button_stop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, filename + " - File already downloaded.", Toast.LENGTH_LONG).show();
                }
            });

        }
    }

    public void updateViewPosition(int position) {
        notifyItemChanged(position);
    }

    private void showCompleteRatingDialog(Context mContext, String type) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(mContext);
        builderSingle.setCancelable(true);
        builderSingle.setTitle("All downloads is complete!");
        if (type.equalsIgnoreCase(COMPLETE)) {
            builderSingle.setMessage("Would you like to give us five starts ⭐⭐⭐⭐⭐ for our app?");

            builderSingle.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String urlApp = "http://play.google.com/store/apps/details?id=" + mContext.getPackageName();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp));
                    mContext.startActivity(browserIntent);
                    SharedPref.saveBol(Constants.RATING_OPTIONS, true);
                }
            });

            builderSingle.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        } else if (type.equalsIgnoreCase(UNCOMPLETE)) {
            builderSingle.setMessage("Found " + listKontenFailed.size() + "media is failed to download. Would you like to re-download again?");

            builderSingle.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    runReDownloadAgain(listKontenFailed);

                }
            });

            builderSingle.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

    }

    private void runReDownloadAgain(List<MediasItem> listKontenFailed) {

        String mBaseFolderPath = android.os.Environment
                .getExternalStorageDirectory()
                + File.separator
                + mContext.getResources().getString(R.string.app_name) + File.separator;
        if (!new File(mBaseFolderPath).exists()) {
            new File(mBaseFolderPath).mkdir();
        }

        for (MediasItem mediasItem : listKontenFailed) {
            String file_url = mediasItem.getUrl();
            assert file_url != null;
            String filename = new File(file_url).getName().replaceFirst("[?][^.]+$", "");
            File file = new File(mBaseFolderPath + "/" + filename);
            int downloadId = PRDownloader.download(mediasItem.getUrl(), mBaseFolderPath, filename)
                    .build()
                    .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                        @Override
                        public void onStartOrResume() {

                        }
                    })
                    .setOnPauseListener(new OnPauseListener() {
                        @Override
                        public void onPause() {

                        }
                    })
                    .setOnCancelListener(new OnCancelListener() {
                        @Override
                        public void onCancel() {

                        }
                    })
                    .setOnProgressListener(new OnProgressListener() {
                        @Override
                        public void onProgress(Progress progress) {

                        }
                    })
                    .start(new OnDownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            downloadCounter = downloadCounter + 1;
                            if (downloadCounter == getItemCount()) {
                                showCompleteRatingDialog(mContext, COMPLETE);
                            }
//
                            DBHelper.setDownload(mContext, mediasItem);


                            //scan media for new file agar bisa diindex
                            if (filename.contains(".mp4")) {
                                MediaScannerConnection.scanFile(mContext,
                                        new String[]{file.getAbsolutePath()}, new String[]{"video/mp4"},
                                        new MediaScannerConnection.OnScanCompletedListener() {
                                            public void onScanCompleted(String path, Uri uri) {
                                                Log.i("ExternalStorage", "Scanned " + path + ":");
                                                Log.i("ExternalStorage", "-> uri=" + uri);
                                            }
                                        });

                                ContentValues values = new ContentValues();

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date date = new Date();
                                values.put(MediaStore.Video.Media.DATE_TAKEN, dateFormat.format(date));
                                values.put(MediaStore.Video.Media.DATE_ADDED, dateFormat.format(date));
                                values.put(MediaStore.Video.Media.DATE_MODIFIED, dateFormat.format(date));
                                values.put(MediaStore.Audio.Media.MIME_TYPE, "video/mp4");
                                values.put(MediaStore.Video.Media.DATA, file.getAbsolutePath());
//                                        //////Log.d("MP3Downloader", "Data Download = "+file.getAbsolutePath());

                                mContext.getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
                            } else {
                                MediaScannerConnection.scanFile(mContext,
                                        new String[]{file.getAbsolutePath()}, new String[]{"image/jpeg", "image/png"},
                                        new MediaScannerConnection.OnScanCompletedListener() {
                                            public void onScanCompleted(String path, Uri uri) {
                                                Log.i("ExternalStorage", "Scanned " + path + ":");
                                                Log.i("ExternalStorage", "-> uri=" + uri);
                                            }
                                        });

                                ContentValues values = new ContentValues();
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date date = new Date();
                                values.put(MediaStore.Images.Media.DATE_TAKEN, dateFormat.format(date));
                                values.put(MediaStore.Images.Media.DATE_ADDED, dateFormat.format(date));
                                values.put(MediaStore.Images.Media.DATE_MODIFIED, dateFormat.format(date));
                                if (filename.contains(".jpg")) {
                                    values.put(MediaStore.Audio.Media.MIME_TYPE, "image/jpeg");
                                } else if (filename.contains(".png")) {
                                    values.put(MediaStore.Audio.Media.MIME_TYPE, "image/png");
                                }

                                values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
//                                        //////Log.d("MP3Downloader", "Data Download = "+file.getAbsolutePath());

                                mContext.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                            }


                        }

                        @Override
                        public void onError(Error error) {
                            listKontenFailed.add(mediasItem);
                            downloadCounter = downloadCounter + 1;
                            if (downloadCounter == getItemCount()) {
                                showCompleteRatingDialog(mContext, UNCOMPLETE);
                            }
                            Toast.makeText(mContext, "Failed to download: " + filename + ". Server message: " + error.getServerErrorMessage(), Toast.LENGTH_SHORT).show();
                        }

                    });
        }
    }

    @Override
    public int getItemCount() {
        return listKonten.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewThumbnail; //, iv_download, iv_more;
        private TextView tv_name, tv_filename;
        private ProgressBar progressBar;
        private ImageButton iv_button_stop;

        public MyViewHolder(View view) {
            super(view);
            imageViewThumbnail = view.findViewById(R.id.imageViewThumbnail);
            tv_name = view.findViewById(R.id.tv_name);
            tv_filename = view.findViewById(R.id.tv_filename);
            progressBar = view.findViewById(R.id.progress_horizontal);
            iv_button_stop = view.findViewById(R.id.iv_button_stop);
        }
    }


}