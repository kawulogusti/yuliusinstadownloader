package com.video.photo.downloader.instagram.bulk.adapter;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.video.photo.downloader.instagram.bulk.R;
import com.video.photo.downloader.instagram.bulk.activity.HistoryActivity;
import com.video.photo.downloader.instagram.bulk.activity.PreviewActivity;
import com.video.photo.downloader.instagram.bulk.activity.UsermediaActivity;
import com.video.photo.downloader.instagram.bulk.model.Download;
import com.video.photo.downloader.instagram.bulk.model.Favorite;
import com.video.photo.downloader.instagram.bulk.model.MediasItem;
import com.video.photo.downloader.instagram.bulk.service.DownloadService;
import com.video.photo.downloader.instagram.bulk.utils.DBHelper;
import com.video.photo.downloader.instagram.bulk.utils.GlideApp;
import com.video.photo.downloader.instagram.bulk.utils.HelperUtils;
import com.video.photo.downloader.instagram.bulk.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ahm on 11/05/17.
 */

public class HistoryItemGridAdapter extends RecyclerView.Adapter<HistoryItemGridAdapter.MyViewHolder> {

    private Context mContext;
    private List<MediasItem> listKonten;
    private HistoryActivity usermediaActivity;

    private boolean isSelectedAllEnabled = false;

    public HistoryItemGridAdapter(Context mContext, List<MediasItem> listKontenP) {
        this.mContext = mContext;
        this.listKonten = listKontenP;
        this.usermediaActivity = (HistoryActivity) mContext;
    }

    public boolean isSelectedAllEnabled() {
        return isSelectedAllEnabled;
    }

    public void setSelectedAllEnabled(boolean selectedAllEnabled) {
        if (selectedAllEnabled) {
            isSelectedAllEnabled = selectedAllEnabled;
            for (int i = 0; i < getItemCount(); i++) {
                listKonten.get(i).setSelected(true);
            }
        } else {
            isSelectedAllEnabled = selectedAllEnabled;
            for (int i = 0; i < getItemCount(); i++) {
                listKonten.get(i).setSelected(false);
            }
        }

    }

    public ArrayList<MediasItem> getSelectedMediaItems() {
        ArrayList<MediasItem> mediasItemsLocal = new ArrayList<>();
        for (MediasItem mediasItem : listKonten) {
            if (mediasItem.isSelected()) {
                LogUtils.log("Found selected: " + mediasItem.getId());
                mediasItemsLocal.add(mediasItem);
            }
        }
        return mediasItemsLocal;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_usermediaitem, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final MediasItem mediasItem = listKonten.get(position);

        GlideApp
                .with(mContext)
                .load(mediasItem.getDisplay_url())
                .error(R.drawable.ic_broken_image_black_24dp)
                .into(holder.iv_image);

        if (mediasItem.getUrl().contains(".mp4")) {
            holder.iv_type.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_videocam_black_24dp));
        } else {
            holder.iv_type.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_image_black_24dp));
        }

        if (isSelectedAllEnabled) {
            holder.cb_selected.setChecked(true);
        } else {
            holder.cb_selected.setChecked(false);
        }

        holder.cb_selected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LogUtils.log("Selected: " + isChecked);
                if (isChecked) {
                    listKonten.get(position).setSelected(true);
                } else {
                    listKonten.get(position).setSelected(false);
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usermediaActivity.showIntersialAds();
                Intent intent = new Intent(mContext, PreviewActivity.class);
                intent.putExtra(PreviewActivity.CURRENT_MEDIA_ITEM, mediasItem);
                intent.putParcelableArrayListExtra(PreviewActivity.MEDIA_LIST, new ArrayList<MediasItem>(listKonten));
                intent.putExtra(PreviewActivity.CURRENT_POSITION, position);
                mContext.startActivity(intent);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showPopupMenu(mediasItem, v, position);
                return true;
            }
        });

    }

    private void showPopupMenu(MediasItem konten, View view, int position) {
        PopupMenu popup = new PopupMenu(mContext, view);
        //inflating menu from xml resource
        popup.inflate(R.menu.menu_item_tweet_grid);
        Menu menuOpts = popup.getMenu();

        Favorite favorite = DBHelper.getFavorite(konten.getId());
        if (favorite != null) {
            menuOpts.getItem(4).setTitle(R.string.text_remove_from_favorite);
        } else {
            menuOpts.getItem(4).setTitle(R.string.text_add_to_favorite);
        }
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_show_profile:
                        //handle menu2 click
                        Intent intent = new Intent(mContext, UsermediaActivity.class);
                        intent.putExtra(UsermediaActivity.USER_NAME, konten.getUserItem());
                        mContext.startActivity(intent);
                        return true;
                    case R.id.action_delete:
                        //handle menu2 click
                        showDeleteDialog(konten, position);
                        return true;
                    case R.id.action_download:
                        //handle menu2 click
                        Intent intentDownload = new Intent(DownloadService.BROADCAST_START_DOWNLOAD_FILE);
                        intentDownload.putExtra(DownloadService.FILE_URL, konten);
                        mContext.sendBroadcast(intentDownload);
                        return true;

                    case R.id.action_show_caption:
                        //handle menu2 click
                        showCaptionDialog(konten.getText());
                        return true;
                    case R.id.action_favorite:
                        //handle menu2 click
                        DBHelper.setOrDeleteFavorite(konten);
                        if (favorite != null) {
                            Toast.makeText(mContext, "Removed from favorite", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, "Added to favorite", Toast.LENGTH_SHORT).show();
                        }
                        notifyDataSetChanged();
                        return true;

                    case R.id.action_share:
                        Download download = DBHelper.getDownload(konten.getId());
                        if (download != null) {
                            shareFile(mContext, HelperUtils.getFilePath(mContext, konten.getUrl()));
                        } else {
                            Toast.makeText(mContext, "Please download first.", Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    case R.id.action_open_twitter:
                        Toast.makeText(mContext, "Opening on Instagram..", Toast.LENGTH_SHORT).show();
                        lauchTwitter(mContext, konten);
                        return true;

                    default:
                        return false;
                }
            }
        });
        //displaying the popup
        popup.show();
    }

    private void showDeleteDialog(MediasItem mediasItem, int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(R.string.text_delete_confirmation_history_item);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DBHelper.deleteHistory(mediasItem);
                Toast.makeText(mContext, "History item deleted", Toast.LENGTH_SHORT).show();
                listKonten.remove(position);
                notifyItemRemoved(position);
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private void showCaptionDialog(String text) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(text)
                .setPositiveButton("Copy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        copyCaption(text);
                    }
                }).show();
    }

    private void copyCaption(String text) {
        ClipboardManager clipboard = (ClipboardManager)
                mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("text", text);
        assert clipboard != null;
        clipboard.setPrimaryClip(clip);
        Toast.makeText(mContext, "Caption copied", Toast.LENGTH_SHORT).show();
    }

    private void shareFile(Context context, String filePath) {
//        //Log.d("VideoDownload", filePath);
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri videoUri = Uri.parse(filePath);
        if (filePath.endsWith(".jpg")) {
            sharingIntent.setType("image/jpg");
        } else if (filePath.endsWith(".png")) {
            sharingIntent.setType("image/png");
        }
        if (filePath.endsWith(".mp4")) {
            sharingIntent.setType("video/mp4");
        } else {
            sharingIntent.setType("image/*");
        }

        sharingIntent.putExtra(Intent.EXTRA_STREAM, videoUri);
        context.startActivity(Intent.createChooser(sharingIntent, "Share media using.."));
    }

    private boolean isIntentAvailable(Context ctx, Intent intent) {
        final PackageManager packageManager = ctx.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    private void lauchTwitter(Context context, MediasItem mediasItem) {

        Uri uri = Uri.parse("http://instagram.com/p/" + mediasItem.getExpandedUrl());
        Intent insta = new Intent(Intent.ACTION_VIEW, uri);
        insta.setPackage("com.instagram.android");

        if (isIntentAvailable(context, insta)) {
            context.startActivity(insta);
        } else {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/p/" + mediasItem.getExpandedUrl())));
        }
    }

    @Override
    public int getItemCount() {
        return listKonten.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_image, iv_type; //, iv_download, iv_more;
        private TextView tv_name, tv_username;
        private CheckBox cb_selected;

        public MyViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.iv_image);
            iv_type = view.findViewById(R.id.iv_type);
            cb_selected = view.findViewById(R.id.cb_selected);
        }
    }


}