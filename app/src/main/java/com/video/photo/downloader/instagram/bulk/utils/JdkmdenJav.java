package com.video.photo.downloader.instagram.bulk.utils;

import android.util.Base64;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class JdkmdenJav {

    private static final int pswdIterations = 10;
    private static final int keySize = 128;
    private static final String cypherInstance = "AES/CBC/PKCS5Padding";
    private static final String secretKeyInstance = "PBKDF2WithHmacSHA1";
    private static final String plainText = xkey();
    private static final String AESSalt = xkeys();
    private static final String initializationVector = "8119745113154120";

    static {
        System.loadLibrary("realm-lib-sok");
    }

    public static native String xkey();

    public static native String xkeys();

    public static native String kdjirj();

//    public static native String jkeey();


    public static String getWorker() {
        /*try {
            Log.d("TwitterDl", "JkeeyTub: "+encrypt(kdjirj()));
            return decryptBase64(kdjirj());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }*/
        try {
//            Log.d("TwitterDl", "Worker: "+decryptBase64(JdkmdenJav.decrypt(kdjirj())));
            return decryptBase64(JdkmdenJav.decrypt(kdjirj()));
        } catch (Exception e) {
            e.printStackTrace();
//            Log.d("MP3Downloader", "Error: "+e.getLocalizedMessage());
            return "";
        }
    }

    /*public static String getJkeey() {
//        Log.d("MP3Downloader", "Jkeey: "+decryptBase64(jkeey()));
        return decryptBase64(jkeey());
    }*/


    public static String encrypt(String textToEncrypt) throws Exception {

        SecretKeySpec skeySpec = new SecretKeySpec(getRaw(plainText, AESSalt), "AES");
        Cipher cipher = Cipher.getInstance(cypherInstance);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(initializationVector.getBytes()));
        byte[] encrypted = cipher.doFinal(textToEncrypt.getBytes());
        return Base64.encodeToString(encrypted, Base64.DEFAULT);
    }

    public static String decrypt(String textToDecrypt) throws Exception {

        byte[] encryted_bytes = Base64.decode(textToDecrypt, Base64.DEFAULT);
        SecretKeySpec skeySpec = new SecretKeySpec(getRaw(plainText, AESSalt), "AES");
        Cipher cipher = Cipher.getInstance(cypherInstance);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(initializationVector.getBytes()));
        byte[] decrypted = cipher.doFinal(encryted_bytes);
        return new String(decrypted, "UTF-8");
    }

    private static byte[] getRaw(String plainText, String salt) {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance(secretKeyInstance);
            KeySpec spec = new PBEKeySpec(plainText.toCharArray(), salt.getBytes(), pswdIterations, keySize);
            return factory.generateSecret(spec).getEncoded();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }


    public static String decryptBase64(String inputString) {
        byte[] actualByte = Base64.decode(inputString, Base64.DEFAULT);
        return new String(actualByte);
    }

}
