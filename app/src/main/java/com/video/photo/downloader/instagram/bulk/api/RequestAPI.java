package com.video.photo.downloader.instagram.bulk.api;

import com.video.photo.downloader.instagram.bulk.model.ResponseMedia;
import com.video.photo.downloader.instagram.bulk.model.ResponseUser;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ahm on 11/05/17.
 */

public interface RequestAPI {

    /*
     * Retrofit get annotation with our URL
     * And our method that will return us details of student.
     */

    @GET("confApp")
    Call<ConfApp> getConfApp(@Query("packageName") String packageName, @Query("versionName") String versionName);

    @GET("ig/user")
    Call<ResponseMedia> getUserMedia(
            @Query("id_username") String id_username, @Query("next") String next
    );

    @GET("ig/post")
    Call<ResponseMedia> getTweetMedia(
            @Query("id") String id
    );

    @GET("ig/search/user")
    Call<ResponseUser> searchUsers(
            @Query("q") String query
    );

    @GET("search/tweet")
    Call<ResponseMedia> searchTweets(
            @Query("q") String query
    );

    @GET("logsPremium")
    Call<ResponseLogs> getLogsPremium(
            @Query("packageName") String packageName, @Query("appVersion") String appVersion, @Query("message") String message
    );

}
